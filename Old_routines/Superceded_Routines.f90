!**************************************************************************************************************!
!                                       Superceded Routines                                                    !
!**************************************************************************************************************!

! This file contains superceded subroutines.
! They have been stored in this file in case we wish to inspect them at a later date.

!===========================================================================================!
!                           Initialize Distance function                                    !
!===========================================================================================!
    !***************************************************************************************!
    ! Module location: initialization.f90
    ! New Routine: init_dist_fnctn_stp_crit
    !
    ! This routine has been superceded by one that uses a stopping criteria to terminate
    ! the iterations.
    !
    ! This routine originally only iterated to a specified amount of iterations.
    !***************************************************************************************!

    subroutine init_dist_fnctn(dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN, imsk)

        !=======================================================================================
        ! This subroutine initializes the distance function over the entire domain.
        !
        ! It solves the distance function by:
        !
        !   1. Looking for boundary points
        !       - if a T point isn't an ice cell, the distance value is initialized to -dx
        !       - if a T point is an ice cell, the distance value is initialized to dx
        !       
        !   2. Solves the following PDE to steady state (or close to steady state)
        !       
        !           phi_t = s(phi_init) (1 - |grad(phi)|),
        !       
        !       where s is a smooth sign function, phi represents the 
        !       distance function, and phi_init is the initialized distance values.
        !       Solving this to near steady state implies that magnitude of the
        !       gradient = 1, which implies that the distance function is solved.
        !       NOTE: the gradient is stored.
        !
        !   3. This subroutine also approximates the distance function at N points through
        !      a bilinear approximation.
        !
        !=======================================================================================

        !==================!
        ! ------IN ------- !
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxT, nyT, nxN, nyN              ! indices limits for T points and N points
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (in) :: &
            imsk                            ! ice extent mask

        !==================!
        ! ---- IN/OUT ---- !
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (inout) :: &
            dist_T, dist_gx_T,  dist_gy_T   ! distance function at T points and gradients
        real(kind = dbl_kind), dimension(nxN, nyN), intent (inout) :: &
            dist_N                          ! distance function at N points

        !==================!
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: &
            i, j, iter_count                    ! local indices and counters
        integer(kind = int_kind), parameter :: &
            max_iter    = 1000                  ! max iteration
        real(kind = dbl_kind), parameter :: &
            eps_dist    = dx,               &   ! used to define a smooth sign function
            dt_dist     = dx/10_dbl_kind        ! "time step" used. This is purely numerical. See Sussman 1994
        real(kind = dbl_kind) :: &
            a, b, c, d,                     &   ! used to calc derivatives
            a_neg, b_neg, c_neg, d_neg,     &   ! used in distance value updates
            a_pos, b_pos, c_pos, d_pos      
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) ::  &
            dist_T_prev,                    &   ! stores the previous iteration's solution
            sign_T                              ! smooth sign function 

        ! -----Initialize Distance Function----- !
        do i = 1-gc_T, nxT+gc_T
            do j = 1-gc_T, nyT+gc_T

                if (imsk(i,j)) then
                    ! if cell doesn't have ice, initialize to -dx

                    dist_T(i, j) = -dx
                    sign_T(i, j) = dist_T(i, j)/sqrt(dist_T(i, j)**2 + eps_dist**2) 

                else
                    ! the cell is an interior point. Set distance value to dx. 
                    
                    dist_T(i, j) = dx
                    sign_T(i, j) = dist_T(i, j)/sqrt(dist_T(i, j)**2 + eps_dist**2) 

                endif

            enddo
        enddo

        ! ---------------------- Solve Distance Function at T points ---------------------------- !
        do iter_count = 1, max_iter

            ! store previous iteration
            dist_T_prev = dist_T

            ! update values everywhere
            do i = 1-gc_T, nxT+gc_T
                do j = 1-gc_T, nyT+gc_T

                !------------ Using scheme in Sussman 1994 ------------------------!
                ! NOTE: the if statements check for boundary conditions at the edge 
                ! domain (including ghost cells)
                !------------------------------------------------------------------!
                    !------------ Calc X derivatives -------------------!
                    if (i == 1-gc_T) then
                        a = (dist_T_prev(i+1, j) - dist_T_prev(i, j))/dx
                    else
                        a = (dist_T_prev(i, j) - dist_T_prev(i-1, j))/dx
                    endif

                    if (i == nxT+gc_T) then 
                        b = (dist_T_prev(i, j) - dist_T_prev(i-1, j))/dx
                    else
                        b = (dist_T_prev(i+1, j) - dist_T_prev(i, j))/dx
                    endif   

                    !------------ Calc Y derivatives -------------------!
                    if (j == 1-gc_T) then 
                        c = (dist_T_prev(i, j+1) - dist_T_prev(i, j))/dx
                    else
                        c = (dist_T_prev(i, j) - dist_T_prev(i, j-1))/dx
                    endif

                    if (j == nyT+gc_T) then
                        d = (dist_T_prev(i, j) - dist_T_prev(i, j-1))/dx
                    else
                        d = (dist_T_prev(i, j+1) - dist_T_prev(i, j))/dx
                    endif

                    a_pos = max(0.0_dbl_kind,a)
                    b_pos = max(0.0_dbl_kind,b)
                    c_pos = max(0.0_dbl_kind,c)
                    d_pos = max(0.0_dbl_kind,d)

                    a_neg = min(0.0_dbl_kind,a)
                    b_neg = min(0.0_dbl_kind,b)
                    c_neg = min(0.0_dbl_kind,c)
                    d_neg = min(0.0_dbl_kind,d)

                    if (dist_T(i,j) > 0) then 
                        dist_T(i,j) = dist_T_prev(i,j) + dt_dist*sign_T(i,j)*(1 - sqrt(max((a_pos**2), (b_neg**2)) + max((c_pos**2), (d_neg**2))))
                    elseif (dist_T(i,j) < 0) then
                        dist_T(i,j) = dist_T_prev(i,j) + dt_dist*sign_T(i,j)*(1 - sqrt(max((a_neg**2), (b_pos**2)) + max((c_neg**2), (d_pos**2))))
                    else
                        dist_T(i,j) = dist_T_prev(i,j)
                    endif

                    !---------------- Store Gradient ---------------------!
                    dist_gx_T(i,j) = (a+b)/2
                    dist_gy_T(i,j) = (c+d)/2
                enddo
            enddo  
        enddo

        ! ------------------ Approximate Distance Function at N points ----------------- !
        do i = 1, nxN
            do j = 1, nyN
                dist_N(i,j) = (dist_T(i,j) + dist_T(i,j-1) + dist_T(i-1,j-1) + dist_T(i-1,j))/4   ! bilinear approximation
            enddo                 
        enddo

    end subroutine init_dist_fnctn

!===========================================================================================!
!                           Set Idealized Initial conditions                                !
!===========================================================================================!
    !***************************************************************************************!
    ! Module location: initialization.f90
    ! New Routine: get_idealized_init_cond_dist
    !
    ! This routine has been superceded by one that uses the solved distance function to 
    ! set initial conditions.
    !
    ! This routine originally only set the initial conditions via logical masks.
    !***************************************************************************************!

    subroutine get_idealized_init_cond(h_T, A_T, ugrid, vgrid, imsk, lmsk, nxT, nyT, nxU, nyU, nxV, nyV)
        
        !=======================================================================================
        ! This subroutine sets the initial thickness and area fraction      
        ! fields. The ice velocities are set to zero across the entire grid 
        ! and h and A are set to zero where land and ice extent masks are.  
        ! At a later date, when the model is running, a different subroutine
        ! should be written to bring in more realistic initial conditions.  
        !                                                                   
        ! Where the ice extent mask is turned off, the thickness is set     
        ! to 4 meters and the area fraction to 1.                           
        !=======================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT,    nyT,        &   ! limit of indices for tracer points (not including ghost cells)
            nxU,    nyU,        &   ! limit of indices for U points (not including ghost cells)
            nxV,    nyV             ! limit of indices for V points (not including ghost cells)
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            lmsk,   imsk 
        
        !==================!
        !------In/out------!
        !==================!
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            h_T,    A_T
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(inout) ::  &   
            ugrid   
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(inout) ::  &
            vgrid

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: i, j                 ! local indices
                    
        ! start ice velocities at zero, across the entire grid
        ugrid = 0   
        vgrid = 0   
        
        ! set A and h (includes ghost cells)
        do j = 1-gc_T, nxT+gc_T
            do i = 1-gc_T, nyT+gc_T
                if (lmsk(i,j) .or. imsk(i,j)) then 
                    h_T(i,j) = 0
                    A_T(i,j) = 0
                else
                    h_T(i,j) = 4    ! initially sets thickness to 4 m
                    A_T(i,j) = 1
                end if
            end do
        end do
        
    end subroutine get_idealized_init_cond  

!===========================================================================================!
!                               COMPRESS INDICES 01                                         !
!===========================================================================================!
    !***************************************************************************************!
    ! Module location: comp_routines.f90
    ! New Routine: compress_ind_dist
    !
    ! This routine has been superceded by one that uses the solved distance function to 
    ! compress the indices.
    !
    ! This routine originally only compressed the indicies via logicals.
    !***************************************************************************************!
    subroutine compress_ind(indxui, indxuj, indxvi, indxvj, &
                        indxTi, indxTj, indxNi, indxNj, &
                        nU_pnts, nV_pnts,  nT_pnts, nN_pnts, &
                        imsk, lmsk, &
                        nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)
        !=====================================================================!
        ! This subroutine creates a compressed index in order to limit 
        ! calculations to the locations where sea ice is located. Doing this
        ! will set the size of our system matrix to be nU_pnts + nV_pnts.
        !
        ! This subroutine also limits the calculations at T and N points.
        !   - T points are contained in the computational domain if
        !     an ice cell is one of its direct neighbours (This will pick up the 
        !     needed ghost cells (1 row) that surround the icepack)
        !   - N points are contained in the computational domain if
        !     they are located on the corner of an ice cell.
        !=====================================================================!
        
        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) ::  &
            nxU,    nyU,        &   ! limit of indices for u points (not including ghost cells)
            nxV,    nyV,        &   ! limit of indices for v points (not including ghost cells)
            nxT,    nyT,        &   ! limit of indices for T points (not including ghost cells)
            nxN,    nyN             ! limit of indices for N points (not including ghost cells)
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) ::  &
            lmsk
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) ::  &
            imsk
        
        !==================!
        !------In/out------!
        !==================!
        integer(kind = int_kind), intent(inout) :: &
            nU_pnts,    nV_pnts,    nT_pnts,    nN_pnts ! number of points to be considered in the computational domain
        integer(kind = int_kind), dimension (nxU*nyU), intent(inout) :: &
            indxui,     indxuj                          ! grid locations of u points in the comp. domain
        integer(kind = int_kind), dimension (nxV*nyV), intent(inout) :: &
            indxvi,     indxvj                          ! grid locations of v points in the comp. domain
        integer(kind = int_kind), dimension ((nxT+2*gc_T)*(nyT+2*gc_T)), intent(inout) :: &    
            indxTi,     indxTj                          ! grid locations of T points in the comp. domain
        integer(kind = int_kind), dimension (nxN*nyN), intent(inout) :: &    
            indxNi,     indxNj                          ! grid locations of N points in the comp. domain

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: &
            i,  j   ! local indices

        !---nU_pnts----!
        nU_pnts = 0
        do j = 1, nyU
            do i = 1, nxU
                if ((imsk(i-1,j) .and. imsk(i,j)) .or. (lmsk(i-1,j) .or. lmsk(i,j))) then
                    ! If there is no ice on either side of the u point or one side is
                    ! a land cell, don't included the u point in computations.      
                else
                    nU_pnts = nU_pnts + 1
                    indxui(nU_pnts) = i
                    indxuj(nU_pnts) = j
                end if
            end do
        end do
        
        !---nV_pnts----!
        nV_pnts = 0
        do j = 1, nyV
            do i = 1, nxV
                if ((imsk(i, j-1) .and. imsk(i, j)) .or. (lmsk(i, j-1) .or. lmsk(i, j))) then
                    ! If there is no ice above or below of the v point or one side is
                    ! a land cell, don't include the v point in the computations.
                else
                    nV_pnts = nV_pnts + 1
                    indxvi(nV_pnts) = i
                    indxvj(nV_pnts) = j
                end if
            end do
        end do
        
        !---nT_pnts----!
        ! NOTE: this currently only checks for one row of ghost cells as only one row will be needed for most computations.
        ! The only place where more will be needed is in smoothing.
        nT_pnts = 0
        do j = 0, nyT + 1
            do i = 0, nxT + 1
                if( (imsk(i-1, j) .and. imsk(i, j-1) .and. imsk(i+1, j) .and. imsk(i, j+1) ) .and. (imsk(i,j)) ) then   
                    ! If none of the surrounding cells contain ice and the (i, j) cell itself
                    ! does not contain ice, then do not include the tracer cell (i,j) in the computational
                    ! domain.
                else
                    nT_pnts = nT_pnts + 1                                   
                    indxTi(nT_pnts) = i
                    indxTj(nT_pnts) = j
                end if
            end do
        end do

        !---nN_pnts-----!
        do j = 1, nyN
            do i = 1, nxN
                if (imsk(i-1, j) .and. imsk(i, j) .and. imsk(i, j-1) .and. imsk(i-1, j-1)) then 
                    ! If none of the T cells surrounding the N cell of interest contain ice
                    ! then do not include node cell (i,j) in the computaitonal domain
                else
                    nN_pnts = nN_pnts + 1                               
                    indxNi(nN_pnts) = i
                    indxNj(nN_pnts) = j
                end if 
            end do                                                  
        end do
    end subroutine compress_ind

!===========================================================================================!
!                               COMPRESS INDICES 02                                         !
!===========================================================================================!
        !*********************************************************************!
        !   Routine location: comp_routines.f90
        !   New routine: name unchanged
        !
        !   Date of modificaiton: November 24th, 2016
        !
        !   The computational domain is set by what u and v points are inside
        !   ice according to the distance function; when selecting these points
        !   I originally made it so that all node and tracer points where
        !   viscosities and ice strength were needed were included, but this 
        !   also included node points and tracer points that were actually
        !   outside ice, so the values of the viscosities would be zero as the 
        !   ice thickness is zero and thus so is the ice strength.
        !
        !   The modified routine now only includes T and N points where the 
        !   distance function is greater than zero, i.e. they are in ice.
        !*********************************************************************!
    subroutine compress_ind_dist(indxui, indxuj, indxvi, indxvj, &
                        indxTi, indxTj, indxNi, indxNj, &
                        nU_pnts, nV_pnts,  nT_pnts, nN_pnts, &
                        dist_T, lmsk, &
                        nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)
        !=====================================================================!
        ! This subroutine creates a compressed index in order to limit 
        ! calculations to the locations where sea ice is located. Doing this
        ! will set the size of our system matrix to be nU_pnts + nV_pnts. 
        ! U and V points are contained in the domain if the distance function
        ! is positive at the point.
        !
        ! This subroutine also limits the calculations at T and N points.
        !   - T points are "near" U or V points that are in the comp. domain
        !   - N points are "near" U or V points that are in the comp. domain
        !=====================================================================!
        
        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) ::  &
            nxU,    nyU,        &   ! limit of indices for u points (not including ghost cells)
            nxV,    nyV,        &   ! limit of indices for v points (not including ghost cells)
            nxT,    nyT,        &   ! limit of indices for T points (not including ghost cells)
            nxN,    nyN             ! limit of indices for N points (not including ghost cells)
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) ::  &
            lmsk
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T
        
        !==================!
        !------In/out------!
        !==================!
        integer(kind = int_kind), intent(inout) :: &
            nU_pnts,    nV_pnts,    nT_pnts,    nN_pnts ! number of points to be considered in the computational domain
        integer(kind = int_kind), dimension (nxU*nyU), intent(inout) :: &
            indxui,     indxuj                          ! grid locations of u points in the comp. domain
        integer(kind = int_kind), dimension (nxV*nyV), intent(inout) :: &
            indxvi,     indxvj                          ! grid locations of v points in the comp. domain
        integer(kind = int_kind), dimension ((nxT+2*gc_T)*(nyT+2*gc_T)), intent(inout) :: &    
            indxTi,     indxTj                          ! grid locations of T points in the comp. domain
        integer(kind = int_kind), dimension (nxN*nyN), intent(inout) :: &    
            indxNi,     indxNj                          ! grid locations of N points in the comp. domain

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: &
            i,  j   ! local indices
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            include_T                                   ! used to specify which tracer points should be included in computations
        logical, dimension (nxN, nyN) :: &
            include_N                                   ! used to specify which node points should be included in computations

        include_T = .false.
        include_N = .false.

        !---nU_pnts----!
        nU_pnts = 0
        do j = 1, nyU
            do i = 1, nxU
                if (((dist_T(i-1,j) + dist_T(i,j))/2 < 0) .or. (lmsk(i-1,j) .or. lmsk(i,j))) then
                    ! If the u point is not with-in the ice pack or is on the edge of a 
                    ! land cell, don't include in the comp. domain    
                else
                    nU_pnts = nU_pnts + 1
                    indxui(nU_pnts) = i
                    indxuj(nU_pnts) = j

                    ! included needed tracer and node points
                    include_T(i-1,j) = .true.
                    include_T(i,j) = .true.
                    include_N(i,j) = .true.
                    include_N(i,j+1) = .true.
                end if
            end do
        end do
        
        !---nV_pnts----!
        nV_pnts = 0
        do j = 1, nyV
            do i = 1, nxV
                if (((dist_T(i,j-1) + dist_T(i,j))/2 < 0) .or. (lmsk(i, j-1) .or. lmsk(i, j))) then
                    ! If the v point is not with-in the ice pack or is on the edge of a 
                    ! land cell, don't include in the comp. domain
                else
                    nV_pnts = nV_pnts + 1
                    indxvi(nV_pnts) = i
                    indxvj(nV_pnts) = j

                    ! include needed tracer and node points
                    include_T(i,j-1) = .true.
                    include_T(i,j) = .true.
                    include_N(i,j) = .true.
                    include_N(i+1,j) = .true.
                end if
            end do
        end do
        
        !---nT_pnts----!
        ! NOTE: this currently only checks for one row of ghost cells as only one row will be needed for most computations.
        ! The only place where more will be needed is in smoothing.
        nT_pnts = 0
        do j = 0, nyT + 1
            do i = 0, nxT + 1
                if(include_T(i,j)) then   
                    ! Calculations are required
                    nT_pnts = nT_pnts + 1                                   
                    indxTi(nT_pnts) = i
                    indxTj(nT_pnts) = j
                else
                    ! Calculations aren't required
                end if
            end do
        end do

        !---nN_pnts-----!
        do j = 1, nyN
            do i = 1, nxN
                if (include_N(i,j)) then
                    ! Calculations are required
                    nN_pnts = nN_pnts + 1                               
                    indxNi(nN_pnts) = i
                    indxNj(nN_pnts) = j
                else
                    ! Calculations aren't required
                end if 
            end do                                                  
        end do
    end subroutine compress_ind_dist

!===========================================================================================!
!                                   CREAT COMP MSK                                          !
!===========================================================================================!
    !***************************************************************************************!
    ! Module location: comp_routines.f90
    ! New Routine: create_comp_msk_dist
    !
    ! This routine has been superceded by one that uses the solved distance function to 
    ! create the computational masks for the matrix product.
    !
    ! This routine originally used logicals.
    !***************************************************************************************!

    subroutine create_comp_msk(ulmsk, uimsk, vlmsk, vimsk, &
                        imsk, lmsk, &
                        indxui, indxuj, indxvi, indxvj, &
                        nV_pnts, nU_pnts, &
                        nxU, nyU, nxV, nyV, nxT, nyT)
    
        !======================================================================
        ! This subroutine creates the computational masks needed in the 
        ! construction of the matrix vector multiplication (Au) and the RHS 
        ! vector b. The arrays ulmsk and vlmsk enforce a dirichlet BC at land
        ! locations (u=v=0), while uimsk and vimsk enforce an Neumann BC at 
        ! open boundaries.
        !
        ! NOTE: in some instances the ulmsk and uismk will both be activated
        ! (set to 0) but the value of ulmsk will override the uimsk value 
        ! (likewise for vlmsk and vimsk).
        !
        ! NOTE: we may be able to reduce the use of these masks as viscosities
        !  will be zero at open boundaries (i.e. 8 different masks may not be
        !  needed)
        !======================================================================
        
        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT,    nyT,        &   ! limit of indices for tracer points
            nxU,    nyU,        &   ! limit of indices for U points
            nxV,    nyV,        &   ! limit of indices for V points
            nU_pnts,            &   ! number of U points in computational domain
            nV_pnts                 ! number of V points in computational domain 
        integer(kind = int_kind), dimension (nxU*nyU), intent(in) :: &
            indxui, indxuj          ! grid locations of u points in comp. domain 
        integer(kind = int_kind), dimension (nxV*nyV), intent(in) :: &
            indxvi, indxvj          ! grid locations of v points in comp. domain 
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            lmsk,   imsk            ! land and ice masks (T points)
        
        !==================!    
        !------In/out------!
        !==================!
        integer(kind = int_kind), dimension (nxU, nyU, 8), intent(inout) :: &
            ulmsk, uimsk            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 8), intent(inout) :: &
            vlmsk, vimsk            ! computational masks for V points
        !==================!    
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: i,j, ij  ! local indices
            
        !---U points---!
            ulmsk = 1
            uimsk = 1
            
        do ij = 1, nU_pnts
                i = indxui(ij)
                j = indxuj(ij)
            
            !--Mask surrounding U-components as needed--!
                !---enforce B.C. at (i,j-1) u-component---!
                if (lmsk(i-1,j-1) .or.      lmsk(i,j-1))    ulmsk(i,j,1) = 0
                if (imsk(i-1,j-1) .and.     imsk(i,j-1))    uimsk(i,j,1) = 0
                
                !---enforce B.C. at (i-1,j) u-component---!
                if (lmsk(i-2,j) .and. .not. imsk(i-1,j))    ulmsk(i,j,2) = 0
                if (imsk(i-1,j))                            uimsk(i,j,2) = 0
                
                !---enforce B.C. at (i+1,j) u-component---!
                if (lmsk(i+1,j) .and. .not. imsk(i,j))      ulmsk(i,j,3) = 0
                if (imsk(i,j))                              uimsk(i,j,3) = 0
                
                !---enforce B.C. at (i,j+1) u-component---!
                if (lmsk(i-1,j+1) .or.      lmsk(i,j+1))    ulmsk(i,j,4) = 0
                if (imsk(i-1,j+1) .and.     imsk(i,j+1))    uimsk(i,j,4) = 0    
                
            !--Mask surrounding V-components as needed--!
                !---enforce B.C. at (i-1,j) v-component---!
                if (lmsk(i-1,j) .or.        lmsk(i-1,j-1))  ulmsk(i,j,5) = 0
                if (imsk(i-1,j) .and.       imsk(i-1,j-1))  uimsk(i,j,5) = 0    
                
                !---enforce B.C. at (i,j) v-component----!
                if (lmsk(i,j) .or.          lmsk(i,j-1))    ulmsk(i,j,6) = 0
                if (imsk(i,j) .and.         imsk(i,j-1))    uimsk(i,j,6) = 0    
                
                !---enforce B.C. at (i-1,j+1) v-component--!
                if (lmsk(i-1,j+1) .or.      lmsk(i-1,j))    ulmsk(i,j,7) = 0
                if (imsk(i-1,j+1) .and.     imsk(i-1,j))    uimsk(i,j,7) = 0    
                
                !---enforce B.C. at (i,j+1) v-component---!
                if (lmsk(i,j+1) .or.        lmsk(i,j))      ulmsk(i,j,8) = 0
                if (imsk(i,j+1) .and.       imsk(i,j))      uimsk(i,j,8) = 0    
        end do
            
        !---V points---!
            vlmsk = 1
            vimsk = 1
        
        do ij = 1, nV_pnts
                i = indxvi(ij)
                j = indxvj(ij)
                
            !--Mask surrounding U-components as needed--!
                !--enforce B.C. at (i,j-1) u-component--!
                if (lmsk(i-1,j-1) .or.      lmsk(i,j-1))    vlmsk(i,j,1) = 0
                if (imsk(i-1,j-1) .and.     imsk(i,j-1))    vimsk(i,j,1) = 0
                
                !--enforce B.C. at (i+1,j-1) u-component--!
                if (lmsk(i,j-1) .or.        lmsk(i+1,j-1))  vlmsk(i,j,2) = 0
                if (imsk(i,j-1) .and.       imsk(i+1,j-1))  vimsk(i,j,2) = 0
                
                !--enforce B.C. at (i,j) u-component--!
                if (lmsk(i-1,j) .or.        lmsk(i,j))      vlmsk(i,j,3) = 0
                if (imsk(i-1,j) .and.       imsk(i,j))      vimsk(i,j,3) = 0
                
                !--enforce B.C. at (i+1,j) u-component--!
                if (lmsk(i,j) .or.          lmsk(i+1,j))    vlmsk(i,j,4) = 0
                if (imsk(i,j) .and.         imsk(i+1,j))    vimsk(i,j,4) = 0
                    
            !--Mask surrounding V-components as needed--!
                !--enforce B.C. at (i,j-1) v-component--!
                if (lmsk(i,j-2) .and. .not. imsk(i,j-1))    vlmsk(i,j,5) = 0
                if (imsk(i,j-1))                            vimsk(i,j,5) = 0
                
                !--enforce B.C. at (i-1,j) v-component--!
                if (lmsk(i-1,j) .or.        lmsk(i-1,j-1))  vlmsk(i,j,6) = 0
                if (imsk(i-1,j) .and.       lmsk(i-1,j-1))  vimsk(i,j,6) = 0
                    
                !--enforce B.C. at (i+1,j) v-component--!
                if (lmsk(i+1,j) .or.        lmsk(i+1,j-1))  vlmsk(i,j,7) = 0
                if (imsk(i+1,j) .and.       lmsk(i+1,j-1))  vimsk(i,j,7) = 0
            
                !--enforce B.C. at (i,j+1) v-component--!
                if (lmsk(i,j+1) .and. .not. imsk(i,j))      vlmsk(i,j,8) = 0
                if (imsk(i,j))                              vimsk(i,j,8) = 0
        end do
    end subroutine create_comp_msk