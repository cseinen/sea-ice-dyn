! This is the main driver for the JFNK solver and includes the main subroutines;
! these could potentially be later moved into a separate module.

program JFNKsolver

! ----------------------------------------------------------------------------!
	!						Module References							!
		!-------------------------------------------------------!
	use modelparams		! includes physical, numerical, and grid parameters
	use initialization		
	use forcing
! ----------------------------------------------------------------------------!
	
implicit none

	!include "mkl_rci.fi"	! for the fmgres routine
		
	real*8 :: &
		time 			= 0	! initial time value
		
	integer :: &
		nxT, nyT,	&	! limit of indices for tracer points
		nxN, nyN, 	& 	! limit of indices for node points
		nxU, nyU, 	& 	! limit of indices for U points
		nxV, nyV,	&	! limit of indices for V points
		nt,			&	! number of time steps
		nU_pnts,	&	! number of u points to be included in the computational domain
		nV_pnts			! number of v points to be included in the computational domain


	integer, allocatable, dimension (:) :: &
		indxui, indxuj,		&	! grid locations of u points in the comp. domain
		indxvi, indxvj			! grid locations of v points in the comp. domain
		!----------------------------------------------------------------------
		! NOTE: The arrays, indxu(v)i and indxu(v)j are allocated once to be 
		! the maximum size needed even though it often won't be needed (unless 
		! the full domain is covered in ice).
		!----------------------------------------------------------------------
			
	real*8, allocatable, dimension (:) :: &
		u_soln, u_soln_prev		! current and previous time-step solution vector
		!----------------------------------------------------------------------
		! NOTE: the solution and residual vectors are allocated once to be the 
		! max size needed. The full exten of the vectors may not be needed at 
		! each time step (unless the full domain is covered in ice).
		!----------------------------------------------------------------------

	integer, allocatable, dimension (:,:,:) :: &
		ulmsk, vlmsk,	&	! land mask for u and v grid points
		uimsk, vimsk		! ice extent mask for u and v grid points
		!----------------------------------------------------------------------
		! NOTE: the 3rd dimension of the arrays u(v)lmsk and u(v)imsk will be
		! will be set to 8 as to allow for masking of 8 different variables
		! in the matrix/vector multiplication involing the system matrix A.
		!----------------------------------------------------------------------
		
	real*8, allocatable, dimension (:,:) :: &
		ugrid,	vgrid,						&	! u and v velocity components on the grid - m/s
		uocn_u,	uocn_v,						&	! u component of ocean current - m/s (needed at u and v grid points)
		vocn_u,	vocn_v,						&	! v component of ocean current - m/s (needed at u and v grid points)
		uwnd_u, uwnd_v,						&	! u component of wind - m/s (needed at u and v grid points)
		vwnd_u,	vwnd_v,						&	! v component of wind - m/s (needed at u and v grid points)
		h_u,	h_v,	h_T,	h_N,		&	! thickness values for the four type of grid points - meters
						A_T,	A_N,		&	! sea ice concentration at T and N grid points - fraction
						P_T,	P_N,		&	! ice strength at T and N grid points - N/m^2
						xi_T,	xi_N,		&	! bulk viscosity - N*s/m^2
						eta_T,	eta_N,		&	! shear viscosity - N*s/m^2
		Cw_u,	Cw_v							! non-linear drag coef
						
	logical, allocatable, dimension (:,:) :: &
		lmsk, imsk							! land and ice extent masks
											! Note: these are located at the T grid points
		
	!-----determine grid limits and allocate arrays-----!
		
	call set_grid_limits(ugrid, vgrid, uocn_u, uocn_v, vocn_u, vocn_v, &		! located in initialization module
						uwnd_u, uwnd_v, vwnd_u, vwnd_v, h_u, h_v, h_T, h_N, A_T, A_N, &
						P_T, P_N, xi_T, xi_N, eta_T, eta_N, Cw_u, Cw_v, &
						indxui, indxuj, indxvi, indxvj, &
						ulmsk, vlmsk, uimsk, vimsk,	&
						nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)
						
	!-----initialize T grid points masks------------------!
		
	call init_T_msk(imsk, lmsk, nxT, nyT)										! located in initialization module
	
	!-----set initial conditions------------!
		
	call get_idealized_init_cond(h_T, A_T, ugrid, vgrid, imsk, lmsk, &			! located in initialization module					
								nxT, nyT, nxU, nyU, nxV, nyV)	
	
	!-----get forcing data------------------!
	
	call get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time, lmsk, &
						nxT, nyT, nxU, nyU, nxV, nyV)
						
	call get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time, &
						nxU, nyU, nxV, nyV)
						
	!-----compress index to limit computations to areas where ice is present---!
	
	call compress_ind(indxui, indxuj, indxvi, indxvj, &
							nU_pnts, nV_pnts, &
							imsk, lmsk, &
							nxU, nyU, nxV, nyV)
	
	!-----create computational masks for matrix vector multiplication and RHS vector construction
	call create_comp_msk(ulmsk, uimsk, vlmsk, vimsk, &
						imsk, lmsk, &
						indxui, indxuj, indxvi, indxvj, &
						nV_pnts, nU_pnts, &
						nxU, nyU, nxV, nyV, nxT, nyT)
	
! -----------------------------------------------------------------------------!
	!							Contains								!
! -----------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!	
	subroutine compress_ind(indxui, indxuj, indxvi, indxvj, &
							nU_pnts, nV_pnts, &
							imsk, lmsk, &
							nxU, nyU, nxV, nyV)
		!======================================================================!
		! This subroutine creates a compressed index in order to limit 
		! calculations to the locations where sea ice is located. Doing this
		! will set the size of our system matrix to be nU_pnts + nV_pnts.
		!
		! QUESTION: Discuss with boualem special boundary cases, 
		! 	ie: ice cell - water cell - ice cell (in one direction)
		!	ie: two separate islands of ice that have their corners touching
		!======================================================================
		implicit none
		
		integer :: i,j ! counters
		
		!----In------!
		integer, intent(in) :: &
			nxU, nyU,	&	! limit of indices for u points
			nxV, nyV		! limit of indices for v points
		logical, dimension (-1:nxT+2, -1:nyT+2), intent(in) :: lmsk
		logical, dimension (0:nxT+1, 0:nyT+1), intent(in) :: imsk
		
		!----Inout-----!
		integer, intent(inout) :: nU_pnts, nV_pnts ! number of points to be considered in the computational domain
		integer, dimension (nxU*nyU), intent(inout) :: indxui, indxuj
		integer, dimension (nxV*nyV), intent(inout) :: indxvi, indxvj
		
		!---nU_pnts----!
		nU_pnts = 0
		do j = 1, nyU
			do i = 1, nxU
				if ((imsk(i-1,j) .and. imsk(i,j)) .or. & 	! If there is no ice on either side of the u point or one side is
					(lmsk(i-1,j) .or. lmsk(i,j))) then		! a land cell, don't included the u point in computations.
				else
					nU_pnts = nU_pnts + 1
					indxui(nU_pnts) = i
					indxuj(nU_pnts) = j
				end if
			end do
		end do
		
		!---nV_pnts----!
		nV_pnts = 0
		do j = 1, nyV
			do i = 1, nxV
				if ((imsk(i,j-1) .and. imsk(i,j)) .or. &		! If there is no ice above or below of the v point or one side is		
					(lmsk(i,j-1) .or. lmsk(i,j))) then		! a land cell, don't include the v point in the computations.
				else
					nV_pnts = nV_pnts + 1
					indxvi(nV_pnts) = i
					indxvj(nV_pnts) = j
				end if
			end do
		end do
	end subroutine compress_ind
!------------------------------------------------------------------------------!
	subroutine create_comp_msk(ulmsk, uimsk, vlmsk, vimsk, &
						imsk, lmsk, &
						indxui, indxuj, indxvi, indxvj, &
						nV_pnts, nU_pnts, &
						nxU, nyU, nxV, nyV, nxT, nyT)
	
		!=======================================================================
		! This subroutine creates the computational masks needed in the 
		! construction of the matrix vector multiplication (Au) and the RHS 
		! vector b. The arrays ulmsk and vlmsk enforce a dirichlet BC at land
		! locations (u=v=0), while uimsk and vimsk enforce an Neumann BC at 
		! open boundaries.
		!
		! NOTE: in some instances the ulmsk and uismk will both be activated
		! (set to 0) but the value of ulmsk will override the uimsk value 
		! (likewise for vlmsk and vimsk).
		!
		! NOTE: we may be able to reduce the use of these masks as viscosities
		!  will be zero at open boundaries (i.e. 8 different masks may not be
		!  needed)
		!=======================================================================
		implicit none
		
		integer :: i,j, ij
		
		!------In------!
		integer, intent(in) :: 	&
			nxT, nyT,			&	! limit of indices for tracer points
			nxU, nyU,			&	! limit of indices for U points
			nxV, nyV,			&	! limit of indices for V points
			nU_pnts,			&	! number of U points in computational domain
			nV_pnts					! number of V points in computational domain
			
		integer, dimension (nxU*nyU), intent(in) :: &
			indxui, indxuj			! grid locations of u points in comp. domain
			
		integer, dimension (nxV*nyV), intent(in) :: &
			indxvi, indxvj			! grid locations of v points in comp. domain
			
		logical, dimension (-1:nxT+2, -1:nyT+2), intent(in) :: &
			lmsk				! land mask (T points)
		
		logical, dimension (0:nxT+1, 0:nyT+1), intent(in) :: &
			imsk				! ice extent mask (T points)
			
		!-----In/out----!
		integer, dimension (nxU, nyU, 8), intent(inout) :: &
			ulmsk, uimsk			! computational masks for U points
			
		integer, dimension (nxV, nyV, 8), intent(inout) :: &
			vlmsk, vimsk			! computational masks for V points
			
		!---U points---!
			ulmsk = 1
			uimsk = 1
			
		do ij = 1, nU_pnts
				i = indxui(ij)
				j = indxuj(ij)
			
			!--Mask surrounding U-components as needed--!
				!---enforce B.C. at (i,j-1) u-component---!
				if (lmsk(i-1,j-1) .or. lmsk(i,j-1)) 		ulmsk(i,j,1) = 0
				if (imsk(i-1,j-1) .and. imsk(i,j-1)) 		uimsk(i,j,1) = 0
				
				!---enforce B.C. at (i-1,j) u-component---!
				if (lmsk(i-2,j) .and. .not. imsk(i-1,j)) 	ulmsk(i,j,2) = 0
				if (imsk(i-1,j)) 							uimsk(i,j,2) = 0
				
				!---enforce B.C. at (i+1,j) u-component---!
				if (lmsk(i+1,j) .and. .not. imsk(i,j)) 		ulmsk(i,j,3) = 0
				if (imsk(i,j)) 								uimsk(i,j,3) = 0
				
				!---enforce B.C. at (i,j+1) u-component---!
				if (lmsk(i-1,j+1) .or. lmsk(i,j+1)) 		ulmsk(i,j,4) = 0
				if (imsk(i-1,j+1) .and. imsk(i,j+1)) 		uimsk(i,j,4) = 0	
				
			!--Mask surrounding V-components as needed--!
				!---enforce B.C. at (i-1,j) v-component---!
				if (lmsk(i-1,j) .or. lmsk(i-1,j-1)) 		ulmsk(i,j,5) = 0
				if (imsk(i-1,j) .and. imsk(i-1,j-1)) 		uimsk(i,j,5) = 0 	
				
				!---enforce B.C. at (i,j) v-component----!
				if (lmsk(i,j) .or. lmsk(i,j-1)) 			ulmsk(i,j,6) = 0
				if (imsk(i,j) .and. imsk(i,j-1)) 			uimsk(i,j,6) = 0	
				
				!---enforce B.C. at (i-1,j+1) v-component--!
				if (lmsk(i-1,j+1) .or. lmsk(i-1,j)) 		ulmsk(i,j,7) = 0
				if (imsk(i-1,j+1) .and. imsk(i-1,j)) 		uimsk(i,j,7) = 0	
				
				!---enforce B.C. at (i,j+1) v-component---!
				if (lmsk(i,j+1) .or. lmsk(i,j)) 			ulmsk(i,j,8) = 0
				if (imsk(i,j+1) .and. imsk(i,j)) 			uimsk(i,j,8) = 0	
		end do
			
		!---V points---!
			vlmsk = 1
			vimsk = 1
		
		do ij = 1, nV_pnts
				i = indxvi(ij)
				j = indxvj(ij)
				
			!--Mask surrounding U-components as needed--!
				!--enforce B.C. at (i,j-1) u-component--!
				if (lmsk(i-1,j-1) .or. lmsk(i,j-1)) 	vlmsk(i,j,1) = 0
				if (imsk(i-1,j-1) .and. imsk(i,j-1)) 	vimsk(i,j,1) = 0
				
				!--enforce B.C. at (i+1,j-1) u-component--!
				if (lmsk(i,j-1) .or. lmsk(i+1,j-1)) 	vlmsk(i,j,2) = 0
				if (imsk(i,j-1) .and. imsk(i+1,j-1)) 	vimsk(i,j,2) = 0
				
				!--enforce B.C. at (i,j) u-component--!
				if (lmsk(i-1,j) .or. lmsk(i,j)) 		vlmsk(i,j,3) = 0
				if (imsk(i-1,j) .and. imsk(i,j)) 		vimsk(i,j,3) = 0
				
				!--enforce B.C. at (i+1,j) u-component--!
				if (lmsk(i,j) .or. lmsk(i+1,j)) 		vlmsk(i,j,4) = 0
				if (imsk(i,j) .and. imsk(i+1,j)) 		vimsk(i,j,4) = 0
					
			!--Mask surrounding V-components as needed--!
				!--enforce B.C. at (i,j-1) v-component--!
				if (lmsk(i,j-2) .and. .not. imsk(i,j-1)) 	vlmsk(i,j,5) = 0
				if (imsk(i,j-1)) 							vimsk(i,j,5) = 0
				
				!--enforce B.C. at (i-1,j) v-component--!
				if (lmsk(i-1,j) .or. lmsk(i-1,j-1))			vlmsk(i,j,6) = 0
				if (imsk(i-1,j) .and. lmsk(i-1,j-1))		vimsk(i,j,6) = 0
					
				!--enforce B.C. at (i+1,j) v-component--!
				if (lmsk(i+1,j) .or. lmsk(i+1,j-1))			vlmsk(i,j,7) = 0
				if (imsk(i+1,j) .and. lmsk(i+1,j-1))		vimsk(i,j,7) = 0
			
				!--enforce B.C. at (i,j+1) v-component--!
				if (lmsk(i,j+1) .and. .not. imsk(i,j))		vlmsk(i,j,8) = 0
				if (imsk(i,j))								vimsk(i,j,8) = 0
		end do
		
	end subroutine create_comp_msk
!------------------------------------------------------------------------------!
end program JFNKsolver