#!/bin/bash

lin_num=71

# Navigate to source code
cd ..

# Copy file for backup 
cp modelparams.f90 modelparams_backup.f90
touch modelparams.f90

# define values to test
vals=(2 4 6 8 10)

# loop through vals and test each variable
for val in "${vals[@]}"
do 
    # update ResInc_tol
    sed -i "${lin_num}s/._dbl_kind/${val}_dbl_kind/" modelparams.f90 

    # compile source code 
    make

    # run solver and store output log
    ./JFNKsolver > output_CRes_taufact_${val}.txt

    # move output files to necessary folders
    cd output
    mkdir 2017_05_15/dx40dt1200/ResInc_Damp/iter1000/fact_${val}/
    mv *.dat 2017_05_15/dx40dt1200/ResInc_Damp/iter1000/fact_${val}/
    cd ..
    mv output_CRes_taufact_${val}.txt output/2017_05_15/dx40dt1200/ResInc_Damp/iter1000/fact_${val}/
    
    echo 
    echo Test with ${val} completed!
    echo
done