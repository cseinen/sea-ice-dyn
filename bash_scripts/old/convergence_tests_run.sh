#!/bin/bash

# This script was written to run the JFNK solver 
# multiple times, but with varying values for 
# disctol_coef_nl

# disctol_coef_nl is found on line number 62
tol_coef_linnum=62

# Navigate to Source Code Folder
cd ..

# Copy file for backup 
cp modelparams.f90 modelparams_backup.f90
touch modelparams.f90

# create array of coef values 
coefs=(+3 +2 +1 -0 -1 -2 -3)

# loop through the different coefficient values 
for coef in "${coefs[@]}"
do
    # update disctol_coef_nl
    sed -i "${tol_coef_linnum}s/1e.._dbl_kind/1e${coef}_dbl_kind/" modelparams.f90 

    # compile source code 
    touch modelparams.f90
    make

    # run solver 
    ./JFNKsolver

    # move output files to necessary folders
    cd output
    mkdir 2017_05_12_ConvTests/dx10dt300/10e${coef}/
    mv *.dat 2017_05_12_ConvTests/dx10dt300/10e${coef}/
    cd ..
    
    echo 
    echo Test with 1e${coef} completed!
    echo 
done