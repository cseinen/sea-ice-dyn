#!/bin/bash

lin_num=63

# Navigate to source code
cd ..

# Copy file for backup 
cp modelparams.f90 modelparams_backup.f90
touch modelparams.f90

# define values to test
vals=(1.25 1.50 1.75 2.00)

# loop through vals and test each variable
for val in "${vals[@]}"
do 
    # update ResInc_tol
    sed -i "${lin_num}s/...._dbl_kind/${val}_dbl_kind/" modelparams.f90 

    # compile source code 
    make

    # run solver and store output log
    ./JFNKsolver > output_CRes_ResIncTol_${val}.txt

    # move output files to necessary folders
    cd output
    mkdir 2017_05_15_ResIncTest/dx40dt1200/${val}/
    mv *.dat 2017_05_15_ResIncTest/dx40dt1200/${val}/
    cd ..
    mv output_CRes_ResIncTol_${val}.txt output/2017_05_15_ResIncTest/dx40dt1200/${val}/
    
    echo 
    echo Test with ${val} completed!
    echo
done