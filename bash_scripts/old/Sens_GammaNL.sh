#!/bin/bash

# This script was written to run the JFNK solver 
# multiple times, but with varying values for 
# disctol_coef_nl in order to test the sensitivity to gamma

# disctol_coef_nl is found on line number 62
tol_coef_linnum=62

# Navigate to Source Code Folder
cd ..

# Copy file for backup 
cp modelparams.f90 modelparams_backup.f90
touch modelparams.f90

# create array of coef values for testing tighter tolerances
coefs=(+2)

# loop through the different coefficient values 
for coef in "${coefs[@]}"
do
    # update disctol_coef_nl
    sed -i "${tol_coef_linnum}s/1e.._dbl_kind/1e${coef}_dbl_kind/" modelparams.f90 

    # compile source code 
    touch modelparams.f90
    make

    # run solver 
    ./JFNKsolver | tee log_GammaNLSens_Fine_1e${coef}.txt

    # move output files to necessary folders after renaming files 
    cd output

        # rename files 
        for f in *.dat 
        do 
            mv -- "$f" "${f%.dat}gamma_nl1e${coef}.dat"
        done

        # move files 
        mkdir 2017_06_15_ValSim_ParamSens/gamma_nl/dx10dt300/1e${coef}/
        mv *.dat 2017_06_15_ValSim_ParamSens/gamma_nl/dx10dt300/1e${coef}/
        cd ..
        mv log* output/2017_06_15_ValSim_ParamSens/gamma_nl/dx10dt300/1e${coef}/
    
    echo 
    echo Test with 1e${coef} completed!
    echo 
done

echo Experiment completed!