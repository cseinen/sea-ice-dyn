! This module contains routines for updating the forcing data. 
! Original Author: Clinton Seinen
!
! The contained routines are:
!           - get_ocn_forcing
!           - get_wnd_forcing
    
module forcing

! ------------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams
    use initialization
! ------------------------------------------------------------------------------!
    implicit none
! ------------------------------------------------------------------------------!
    !                       Contains                                    !
! ------------------------------------------------------------------------------!
    
contains
!===========================================================================================!
!                               Get Idealized Ocean Forcing                                 !
!===========================================================================================!
    subroutine get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time, nxT, nyT, nxU, nyU, nxV, nyV)
        !=======================================================================
        ! This subroutine sets the ocean forcing data according to an 
        ! idealized gyre from "Viscous-Plastic Sea Ice Dynamics with the EVP
        ! Model: Linearization Issues" - Elizabeth Hunke, 2000.
        !
        ! Note that this current gyre formulation is constant, so it could be
        ! defined once in the main program and then never updated, but I created
        ! this subroutine because at a later date we will likely want to be 
        ! a time dependent ocean current. Until that point, this routine will
        ! only be called once, in the initialization portion of the JFNK solver
        !======================================================================
        
        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT,    nyT,       &   ! limit of indices for tracer points (not including ghost cells)
            nxU,    nyU,       &   ! limit of indices for U points (not including ghost cells)
            nxV,    nyV            ! limit of indices for V points (not including ghost cells)
        real(kind = dbl_kind), intent(in) :: &
            time

        !==================!
        !-------In/out-----!
        !==================!
        real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: & 
            uocn_u, vocn_u
        real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
            uocn_v, vocn_v

        !==================!
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: &
            i,  j   ! local indices
        real(kind = dbl_kind) :: &
            x,  y   ! local variables to calculate grid location
        
        ! ----- U grid points -----!
        do j = 1, nyU
            do i = 1, nxU
            
                x = (i-1)*dx                                        ! calculate position on grid    
                y = (j-1)*dx + dx/2
                
                if (lmsk(i-1, j) .or. lmsk(i, j)) then              ! check for land on either side of the u grid point
                    uocn_u(i,j) = 0                                 ! and set ocean velocities to 0 when land is present
                    vocn_u(i,j) = 0     
                else
                    uocn_u(i,j) = 0.1*(2*y - y_extent)/y_extent
                    vocn_u(i,j) = -0.1*(2*x - x_extent)/x_extent
                endif
            
            enddo
        enddo
        
        ! ----- V grid points -----!
        do j = 1, nyV
            do i = 1, nxV
            
            x = (i-1)*dx + dx/2                                     ! calculate position on grid
            y = (j-1)*dx        
            
                if (lmsk(i, j-1) .or. lmsk(i, j)) then              ! check for land above and below v grid point
                    uocn_v(i,j) = 0                                 ! and set ocean velocities to 0 when land is present
                    vocn_v(i,j) = 0     
                else
                    uocn_v(i,j) = 0.1*(2*y - y_extent)/y_extent
                    vocn_v(i,j) = -0.1*(2*x - x_extent)/x_extent
                endif
            
            enddo
        enddo
    end subroutine get_ocn_forcing
    
!===========================================================================================!
!                       Get idealized wind forcing                                          !
!===========================================================================================!
    subroutine get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time, nxU, nyU, nxV, nyV)
        !================================================================
        ! This subroutine sets the ocean forcing data according to an 
        ! wind field from "Viscous-Plastic Sea Ice Dynamics with the EVP
        ! Model: Linearization Issues" - Elizabeth Hunke, 2000.
        !
        ! At a later date, this should be updated to use actual wind data.
        !===============================================================
            
            !==================!
            !--------In--------!
            !==================!
            integer(kind = int_kind), intent(in) ::&
                nxU,    nyU,   &       ! limit of indices for U points (not including ghost cells)
                nxV,    nyV            ! limit of indices for V points (not including ghost cells)
            real(kind = dbl_kind), intent(in) :: &
                time
            
            !==================!
            !------In/out------!
            !==================!
            real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: &
                uwnd_u, vwnd_u
            real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
                uwnd_v, vwnd_v
            
            !==================!
            ! -- LOCAL VARS -- !
            !==================!
            integer(kind = int_kind) :: &
                i,  j                   ! local indices
            real(kind = dbl_kind) :: &
                x,  y                   ! local variables to calculate grid location
            real(kind = dbl_kind) :: &
                period = 4*24*60*60     ! four days, in seconds

        !----- U grid points ------!
        do j = 1, nyU
            do i = 1, nxU
                
            x = (i-1)*dx                        ! calculate position on grid
            y = (j-1)*dx + dx/2 
            
            uwnd_u(i,j) = 5 + (sin(2*pi*time/period) - 3)*sin(2*pi*x/x_extent)*sin(pi*y/y_extent)
            vwnd_u(i,j) = 5 + (sin(2*pi*time/period) - 3)*sin(2*pi*y/y_extent)*sin(pi*x/x_extent)
            
            enddo
        enddo
        
        !----- V grid points -------!
        do j = 1, nyV
            do i = 1, nxV
            
            x = (i-1)*dx + dx/2
            y = (j-1)*dx
            
            uwnd_v(i,j) = 5 + (sin(2*pi*time/period) - 3)*sin(2*pi*x/x_extent)*sin(pi*y/y_extent)
            vwnd_v(i,j) = 5 + (sin(2*pi*time/period) - 3)*sin(2*pi*y/y_extent)*sin(pi*x/x_extent)
            
            enddo
        enddo
    end subroutine get_wnd_forcing
end module forcing
