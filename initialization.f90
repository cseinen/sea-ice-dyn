! This module contains various initialization subroutines and functions
! Original Author: Clinton Seinen
!
! The contained routines are:
!           - set_grid_limits_and_initialize
!           - init_T_msk
!           - get_idealized_init_cond_dist
!           - toy_velocity_field
!           - init_dist_fnctn_stp_crit

module initialization

! ------------------------------------------------------------------------------!   
    !                       Module References                           !
    use modelparams     ! includes physical, numerical, and grid parameters
    use csv_file        ! module for writing data to csv format files 
! ------------------------------------------------------------------------------!
    
implicit none   
!-----------------------------------------------------------------------------------------------------!
    !                           Grid Parameters                                     !    
    ! NOTE: These will remain unchanged after one call of set_grid_limits           !
! ----------------------------------------------------------------------------------------------------!

real(kind = dbl_kind), allocatable, dimension (:) :: &
        x_u,    x_v,    x_T,    x_N,        &   ! x positions
        y_u,    y_v,    y_T,    y_N             ! y positions
logical, allocatable, dimension (:,:) :: &
        lmsk,   imsk                            ! land and initial ice extent mask, located at T points

! ------------------------------------------------------------------------------!
    !                           Contains                                !
! ------------------------------------------------------------------------------!
contains
!===========================================================================================!
!                           Set grid limits and allocate arrays                             !
!===========================================================================================!
    subroutine set_grid_limits_and_initialize(ugrid, vgrid, uResgrid, vResgrid, uocn_u, uocn_v, vocn_u, vocn_v, &
                                            uwnd_u, uwnd_v, vwnd_u, vwnd_v, h_u, h_v, h_T, A_T,  &
                                            P_u, P_v, P_T, P_N, zeta_u, zeta_v, eta_u, eta_v, Cw_u, Cw_v,  &
                                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                            dist_T, dist_N, dist_gx_T, dist_gy_T, dist_gx_T_prev, dist_gy_T_prev,  &
                                            indxUi, indxUj, indxVi, indxVj, prevUDom, prevVDom, &
                                            indxTi, indxTj, indxNi, indxNj, indxAdvVi, indxAdvVj, &
                                            ulmsk, vlmsk, uimsk, vimsk, &
                                            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, &
                                            Au, res_NL, u_update, b, b_0)
        !=======================================================================================
        ! This subroutine sets the grid limits for an idealized rectangular 
        ! domain. The variables x_extent, y_extent, and dx come from the    
        ! modelparams module. 
        !
        ! This subroutine also populates the position matrices for all points, this is done  to 
        ! facilitate plotting.
        ! 
        ! NOTE: the size of some arrays are increased to facillitate ghost cells; see 
        ! modelparams.f90 for amount of ghost cells for each type.
        !
        ! NOTE: the matrix vector product, Au, Res_nonlin, u_update, b, b_lin are 
        ! now allocated to their full size, as if the entire domain is covered in ice. 
        !
        ! NOTE: if we want to consider a domain without land surrounding it, we may need to add 
        ! ghost points for the node points.
        !=======================================================================================
        
        !==================!
        !-------In/out-----!
        !==================!
        integer(kind = int_kind), allocatable, dimension (:), intent(inout) :: &
            indxUi,     indxUj,                         &   ! grid locations of u points in the comp. domain
            indxVi,     indxVj,                         &   ! grid locations of v points in the comp. domain
            indxTi,     indxTj,                         &   ! grid locations of T points in the comp. domain
            indxNi,     indxNj,                         &   ! grid locations of N points in the comp. domain    
            indxAdvVi,  indxAdvVj                           ! grid locations of control volumes for advection scheme (node points)
        real(kind = dbl_kind), allocatable, dimension (:), intent(inout) :: &
            Au,         res_NL,     u_update,           &   ! Matrix vector product, non linear residual, velocity update
            b,          b_0                                 ! the RHS vector of the Au = b equation, and the corresponding linear and non-linear parts
        integer(kind = int_kind), allocatable, dimension (:,:,:), intent(inout) ::&
            ulmsk,      vlmsk,                          &   ! land mask for u and v grid points
            uimsk,      vimsk                               ! ice extent mask for u and v grid points
        real(kind = dbl_kind), allocatable, dimension (:,:), intent(inout) :: &
            ugrid,      vgrid,                                  &   ! u and v velocity components on the grid
            uResgrid,   vResgrid,                               &   ! residual on the grid
            uocn_u,     uocn_v,                                 &   ! u component of ocean current (needed at u and v grid points)
            vocn_u,     vocn_v,                                 &   ! v component of ocean current (needed at u and v grid points)
            uwnd_u,     uwnd_v,                                 &   ! u component of wind (needed at u and v grid points)
            vwnd_u,     vwnd_v,                                 &   ! v component of wind (needed at u and v grid points)
            h_u,        h_v,        h_T,                        &   ! thickness values for u, v, and T points 
                                    A_T,                        &   ! sea ice concentration at T points
            P_u,        P_v,        P_T,            P_N,        &   ! ice strength at all point types
            zeta_u,     zeta_v,                                 &   ! bulk viscosity at u and v grid points 
            zeta_gx_u,                                          &   ! x component of the bulk visc grad at U points 
                        zeta_gy_v,                              &   ! y component of the bulk visc grad at V points 
            eta_u,      eta_v,                                  &   ! shear viscosity at u and v grid points 
            eta_gx_u,   eta_gx_v,                               &   ! x component of the shear visc grad at U and V points 
            eta_gy_u,   eta_gy_v,                               &   ! y component of the shear visc grad at U and V points
            Cw_u,       Cw_v,                                   &   ! non-linear drag coef at u and v points 
                                    dist_T,         dist_N,     &   ! distance to the ice pack boundary at T and N points 
                                    dist_gx_T,                  &   ! x component of gradient of distance function solution (tracer points)
                                    dist_gy_T,                  &   ! y component of gradient of distance function solution (tracer points)
                                    dist_gx_T_prev,             &   ! x component of gradient of distance function solution from the previous timestep (tracer points)
                                    dist_gy_T_prev                  ! y component of gradient of distance function solution from the previous timestep (tracer points)
        integer(kind = int_kind), intent(inout) :: &
            nxT,    nyT,                                &   ! limit of indices for tracer points
            nxN,    nyN,                                &   ! limit of indices for node points
            nxU,    nyU,                                &   ! limit of indices for U points
            nxV,    nyV                                     ! limit of indices for V points

        logical, allocatable, dimension (:,:), intent (inout) :: &
            prevUDom, prevVDom                              ! previous timestep computational domain location for U and V points

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: i, j, max_size  ! local indices and maximum number of points in the domain

        nxT = int(x_extent/dx)
        nyT = int(y_extent/dx)
            
        nxN = int(x_extent/dx) + 1
        nyN = int(y_extent/dx) + 1
            
        nxU = int(x_extent/dx) + 1
        nyU = int(y_extent/dx)
            
        nxV = int(x_extent/dx)
        nyV = int(y_extent/dx) + 1

        max_size = nxU*nyU + nxV*nyV
        
        allocate(uocn_u(nxU, nyU), vocn_u(nxU, nyU), uResgrid(nxU, nyU), &                                  ! u-gridpoint arrays
                uwnd_u(nxU, nyU), vwnd_u(nxU, nyU), h_u(nxU, nyU), Cw_u(nxU, nyU), &
                P_u(nxU, nyU), eta_u(nxU, nyU), zeta_u(nxU, nyU), &
                zeta_gx_u(nxU, nyU), eta_gx_u(nxU, nyU), eta_gy_u(nxU, nyU), &
                ulmsk(nxU, nyU, 20), uimsk(nxU, nyU, 20))
        
        allocate(uocn_v(nxV, nyV), vocn_v(nxV, nyV), vResgrid(nxV, nyV), &                                  ! v-gridpoint arrays
                uwnd_v(nxV, nyV), vwnd_v(nxV, nyV), h_v(nxV, nyV), Cw_v(nxV, nyV), &
                P_v(nxV, nyV), eta_v(nxV, nyV), zeta_v(nxV, nyV), &
                zeta_gy_v(nxV, nyV), eta_gx_v(nxV, nyV), eta_gy_v(nxV, nyV), &
                vlmsk(nxV, nyV, 20), vimsk(nxV, nyV, 20))
                        
        allocate(P_N(nxN, nyN), dist_N(nxN, nyN), &                                     ! N-grid point arrays
                indxNi(nxN*nyN), indxNj(nxN*nyN), indxAdvVi(nxN*nyN), indxAdvVj(nxN*nyN), &
                x_N(nxN), y_N(nyN))

        ! allocate arrays for variables where ghost cells are needed

        allocate(ugrid( 1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U ), &                           ! u-gridpoint arrays
                prevUDom( 1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U ), &
                x_u(1-gc_U:nxU+gc_U), y_u(1-gc_U:nyU+gc_U), &
                indxUi((nxU+2*gc_U)*(nyU+2*gc_U)), indxUj((nxU+2*gc_U)*(nyU+2*gc_U)))

        allocate(vgrid( 1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V ), &                           ! v-gridpoint arrays      
                prevVDom( 1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V ), &
                x_v(1-gc_V:nxV+gc_V), y_v(1-gc_V:nyV+gc_V), &
                indxVi((nxV+2*gc_V)*(nyV+2*gc_V)), indxVj((nxV+2*gc_V)*(nyV+2*gc_V)))

        allocate(h_T( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), A_T( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), P_T( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), &               ! T-gridpoint arrays. NOTE: up to 3 rows of ghost cells are available for smoothing purposes
                dist_T( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), dist_gx_T( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), dist_gy_T( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), &                         
                dist_gx_T_prev( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), dist_gy_T_prev( 1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T ), & 
                indxTi((nxT+2*gc_T)*(nyT+2*gc_T)), indxTj((nxT+2*gc_T)*(nyT+2*gc_T)), &
                x_T(1-gc_T:nxT+gc_T), y_T(1-gc_T:nyT+gc_T ))

        ! allocate vectors needed in the solver
        allocate(Au(max_size), res_NL(max_size), u_update(max_size), &
                b(max_size), b_0(max_size))

        ! initialize arrays 
        indxUi  = 0
        indxUj  = 0
        indxVi  = 0
        indxVj  = 0
        indxNi  = 0
        indxNj  = 0
        indxTi  = 0
        indxTj  = 0
        indxAdvVi = 0
        indxAdvVj = 0

        Au       = 0.
        res_NL   = 0.
        u_update = 0.
        b        = 0.
        b_0      = 0.

        ulmsk = 0
        vlmsk = 0

        uimsk = 0
        vimsk = 0

        ugrid       = 0.
        vgrid       = 0.
        uResgrid    = 0.
        vResgrid    = 0.
        uocn_u      = 0.
        uocn_v      = 0.
        vocn_u      = 0.
        vocn_v      = 0.
        uwnd_u      = 0.
        uwnd_v      = 0.
        vwnd_u      = 0.
        vwnd_v      = 0.
        h_u         = 0.
        h_v         = 0.
        h_T         = 0.
        A_T         = 0.
        P_u         = 0.
        P_v         = 0.
        P_T         = 0.
        P_N         = 0.
        zeta_u      = 0.
        zeta_v      = 0.
        zeta_gx_u   = 0.
        zeta_gy_v   = 0.
        eta_u       = 0.
        eta_v       = 0.
        eta_gx_u    = 0.
        eta_gx_v    = 0.
        eta_gy_u    = 0.
        eta_gy_v    = 0.
        Cw_u        = 0.
        Cw_v        = 0.
        dist_T      = 0.
        dist_N      = 0.
        dist_gx_T   = 0.
        dist_gy_T   = 0.

        dist_gx_T_prev  = 0.
        dist_gy_T_prev  = 0.

        prevUDom    = .False.
        prevVDom    = .False.

        ! calculate x and y position grids
        !------U points --------!
        do i = 1 - gc_U, nxU + gc_U
            x_u(i)= (i-1)*dx
        end do
        do j = 1 - gc_U, nyU + gc_U
            y_u(j) = (j-1)*dx + dx/2
        end do

        !-----V points ---------!
        do i = 1 - gc_V, nxV + gc_V
            x_v(i) = (i-1)*dx + dx/2
        end do
        do j = 1 - gc_V, nyV + gc_V
            y_v(j) = (j-1)*dx
        end do

        !-------N points--------!
        do i = 1, nxN
            x_N(i) = (i-1)*dx
        end do
        do j = 1, nyN
            y_N(j) = (j-1)*dx
        end do

        ! -------T points -------!
        do i = 1-gc_T, nxT+gc_T
            x_T(i) = (i-1)*dx + dx/2
        end do
        do j = 1-gc_T, nyT+gc_T
            y_T(j) = (j-1)*dx + dx/2
        end do
            
    end subroutine set_grid_limits_and_initialize

!===========================================================================================!
!                           Initialize Ice and Land masks                                   !
!===========================================================================================!
    subroutine init_T_msk(nxT, nyT) 
        !=======================================================================================
        ! This subroutine sets the idealized domain using the logical masks, lmsk and imsk, to 
        ! specify where we want ice and land.
        !
        ! lmsk = .true. - land is present
        ! imsk = .true. - NO ice is present
        !
        ! Once these masks are set, they are then used to initialize the distance function (in init_dist_fnctn_stp_crit)
        ! which is then used to limit the computational domain and set the initial conditions.
        !
        ! Note that the lmsk and imsk are given extra size to their 
        ! dimensions for numerical purposes. 
        !
        ! CONFIGURATION 1
        !   - land masks around the northern, southern, and eastern borders.
        !   - ice is placed in the SW and NE corners.
        !
        ! CONFIGURATION 2
        !   - no land boundaries
        !   - ice is placed throughout the entire grid (besides locations that will be lost 
        !     due to numerical diffusion in the distance function).
        !
        ! CONFIGURATION 3
        !   - land boundaries along north and south borders, just outside the normal domain. Only 
        !     ghost cells are set. This configuration was implemented to allow for better convergence 
        !     testing where the actual domain of interest isn't altered by the size of land cells.
        !   - ice is placed throughout the entire grid
        !
        ! CONFIGURATION 4
        !   - land boundaries along the East and West borders, just outside the normal domain. Only 
        !     ghost cells are set. This configuration was implemented to help assess the behaviour
        !     at boundaries.
        !   - ice is placed throughout the entire grid
        !
        ! CONFIGURATION 5
        !   - land boundaries along East, West, North, and Southern borders, just outside the 
        !     normal domain.
        !   - ice is placed throughout entiregrid
        !
        ! CONFIGURATION 6
        !   - no land boundaries
        !   - ice in SW and NE corners.
        !
        ! CONFIGURATION 7
        !   - land boudaries along all boundaries, just outside the domain (like config 5)
        !   - ice in SW and NE corners.
        !
        ! CONFIGURATION 8
        !   - no land boundaries
        !   - ice in SW and NE corners but ice is removed from the center of the domain; 
        !     this was done to avoid the singularity seen in the viscosities for 
        !     the validation problem, to better estimate convergence behaviour.
        !
        ! CONFIGURATION 9
        !   - land boundaries along entire domain
        !   - ice in SW and NE corners but ice is removed from the center of the domain; 
        !     this was done to avoid the singularity seen in the viscosities for 
        !     the validation problem, to better estimate convergence behaviour.
        !=======================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT,    nyT        ! limit of indices for tracer points (not including ghost cells)

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: &
            i, j                ! local indices

        real(kind = dbl_kind) :: &
            sing_cutoff,            &   ! defines how far away from the center of the domain ice should be placed
            cnst1, cnst2, cnst3, cnst4  ! constants to help set domains

        sing_cutoff = 160e3     ! don't place ice with-in 160 km of center of domain
        
        allocate(lmsk(1-gc_T:nxT+gc_T,1-gc_T:nyT+gc_T), imsk(1-gc_T:nxT+gc_T,1-gc_T:nyT+gc_T))  
        
        lmsk = .false.                  ! initialize whole array to false (no land)
        imsk = .true.                   ! initialize whole array to true (no ice)
        
        if (domainconfig == 1) then     !***** config 1 *****!
            ! Ice Extent Mask !
            do j = 1, nyT/2
                do i = 1, nxT/2 
                    imsk(i,j) = .false.     ! puts ice in southwest corner of domain
                end do
            end do
            do j = nyT/2 + 1, nyT
                do i = nxT/2 + 1, nxT
                    imsk(i,j) = .false.     ! puts ice in northeast corner of domain
                end do
            end do
            ! Land Mask !
            do i = 1,nxT                
                lmsk(i,nyT) = .true.        ! set land mask along the north border.
                imsk(i,nyT) = .true.        ! no ice on land
                
                lmsk(i,1) = .true.          ! set land mask along south border.
                imsk(i,1) = .true.          ! no ice on land
            end do
            do j = 2, nyT-1
                lmsk(nxT,j) = .true.        ! set land mask along the eastern border.
                imsk(nxT,j) = .true.        ! no ice on land
            end do  

        else if (domainconfig == 2) then !***** config 2 *****!
            ! Ice Extent Mask !
            do j = 1, nyT
                do i = 1, nxT
                    imsk(i,j) = .false.
                end do 
            end do 

        else if (domainconfig == 3) then !***** config 3 *****!
            ! Ice Extent Mask !
            do j = 1, nyT
                do i = 1, nxT
                    imsk(i,j) = .false.
                end do 
            end do 
            ! Land Masks !
            do i = 1, nxT
                lmsk(i,0)       = .true.
                lmsk(i,nyT+1)   = .true.
            end do 

        else if (domainconfig == 4) then !**** config 4 ****!
            ! Ice Extent Mask !
            do j = 1, nyT
                do i = 1, nxT
                    imsk(i,j) = .false.
                end do 
            end do 
            ! Land Mask !
            do j = 1, nxT
                lmsk(0,j)       = .true.
                lmsk(nxT+1,j)   = .true.
            end do

        else if (domainconfig == 5) then !**** config 5 ****!
            ! Ice Extent Mask !
            do j = 1, nyT
                do i = 1, nxT
                    imsk(i,j) = .false.
                end do 
            end do 
            ! Land Mask !
            do j = 1, nxT ! East/West Border 
                lmsk(0,j)       = .true.
                lmsk(nxT+1,j)   = .true.
            end do
            do i = 1, nxT ! North/South Borders
                lmsk(i,0)       = .true.
                lmsk(i,nyT+1)   = .true.
            end do

        else if (domainconfig == 6) then !**** config 6 ****!
            ! Ice Extent Mask !
            do j = 1, nyT/2
                do i = 1, nxT/2 
                    imsk(i,j) = .false.     ! puts ice in southwest corner of domain
                end do
            end do
            do j = nyT/2 + 1, nyT
                do i = nxT/2 + 1, nxT
                    imsk(i,j) = .false.     ! puts ice in northeast corner of domain
                end do
            end do
            
        else if (domainconfig == 7) then !**** config 7 ****!
            ! Ice Extent Mask !
            do j = 1, nyT/2
                do i = 1, nxT/2 
                    imsk(i,j) = .false.     ! puts ice in southwest corner of domain
                end do
            end do
            do j = nyT/2 + 1, nyT
                do i = nxT/2 + 1, nxT
                    imsk(i,j) = .false.     ! puts ice in northeast corner of domain
                end do
            end do
            ! Land Mask !
            do j = 1, nxT ! East/West Border 
                lmsk(0,j)       = .true.
                lmsk(nxT+1,j)   = .true.
            end do
            do i = 1, nxT ! North/South Borders
                lmsk(i,0)       = .true.
                lmsk(i,nyT+1)   = .true.
            end do

        else if (domainconfig == 8) then !**** config 8 ****!
            cnst1 = (x_extent)/2 - sing_cutoff
            cnst2 = (y_extent)/2 - sing_cutoff
            cnst3 = (x_extent)/2 + sing_cutoff
            cnst4 = (y_extent)/2 + sing_cutoff

            ! Ice Extent Mask !
            do j = 1, nyT/2
                do i = 1, nxT/2 
                    if ((x_T(i) < cnst1) .and. (y_T(j) < cnst2)) then
                        imsk(i,j) = .false.     ! puts ice in southwest corner of domain
                    else
                        ! Don't place ice here, avoid the center of the domain 
                    end if 
                end do
            end do
            do j = nyT/2 + 1, nyT
                do i = nxT/2 + 1, nxT
                    if ((x_T(i) > cnst3) .and. (y_T(j) > cnst4)) then 
                        imsk(i,j) = .false.     ! puts ice in northeast corner of domain
                    else
                        ! Don't place ice here, avoid the center of the domain 
                    end if 
                end do
            end do

        else if (domainconfig == 9) then !**** config 9 ****!
            cnst1 = (x_extent)/2 - sing_cutoff
            cnst2 = (y_extent)/2 - sing_cutoff
            cnst3 = (x_extent)/2 + sing_cutoff
            cnst4 = (y_extent)/2 + sing_cutoff

            ! Ice Extent Mask !
            do j = 1, nyT/2
                do i = 1, nxT/2 
                    if ((x_T(i) < cnst1) .and. (y_T(j) < cnst2)) then
                        imsk(i,j) = .false.     ! puts ice in southwest corner of domain
                    else
                        ! Don't place ice here, avoid the center of the domain 
                    end if 
                end do
            end do
            do j = nyT/2 + 1, nyT
                do i = nxT/2 + 1, nxT
                    if ((x_T(i) > cnst3) .and. (y_T(j) > cnst4)) then 
                        imsk(i,j) = .false.     ! puts ice in northeast corner of domain
                    else
                        ! Don't place ice here, avoid the center of the domain 
                    end if 
                end do
            end do

            ! Land Mask !
            do j = 1, nxT ! East/West Border 
                lmsk(0,j)       = .true.
                lmsk(nxT+1,j)   = .true.
            end do
            do i = 1, nxT ! North/South Borders
                lmsk(i,0)       = .true.
                lmsk(i,nyT+1)   = .true.
            end do

        else 
            print *, "Unknown domain configuration. Aborting Run. See init_T_msk in initialization module."
            stop 
        endif  
    
    end subroutine init_T_msk

!===========================================================================================!
!                           Set Idealized Initial conditions                                !
!===========================================================================================!
    subroutine get_idealized_init_cond_dist(h_T, A_T, ugrid, vgrid, dist_T, nxT, nyT, nxU, nyU, nxV, nyV, &
                                            indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts)
        
        !=======================================================================================
        ! This subroutine sets the initial thickness and area fraction      
        ! fields. The ice velocities are set to zero across the entire grid, unless the validation
        ! problem is being tested. h and A are set to zero where land and ice extent masks are activated.   
        !
        ! If init_h_homo is set to true, this routine makes h = 1 every where there is ice. If not 
        ! it uses undulating sine waves (values vary between 2.1 and 0.1 m).                     
        !=======================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT,    nyT,        &   ! limit of indices for tracer points (not including ghost cells)
            nxU,    nyU,        &   ! limit of indices for U points (not including ghost cells)
            nxV,    nyV,        &   ! limit of indices for V points (not including ghost cells)
            nU_pnts, nV_pnts        ! number of U and V points in the computational domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj          ! locations of U points in the computational domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj          ! locations of V points in the computational domain
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T
        
        !==================!
        !------In/out------!
        !==================!
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            h_T,    A_T
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(inout) ::  &   
            ugrid   
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(inout) ::  &
            vgrid

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij  ! local indices
        real(kind = dbl_kind) :: &
            alpha, C    ! useful values for setting varying thickness field 

            C = 2*sqrt(2.)*pi/x_extent
        
        if (.not. valid_pblm) then
            ! start ice velocities at zero, across the entire grid
            ugrid = 0.   
            vgrid = 0. 
        else
            ! validation problem is being executed; load the known initial conditions
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ugrid(i,j) = (1./10)*sin( (4*x_u(i)/x_extent - 2)**2 + (4*y_u(j)/y_extent - 2)**2 )
            end do 
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vgrid(i,j) = (1./10)*cos( (4*x_v(i)/x_extent - 2)**2 + (4*y_v(j)/y_extent - 2)**2 )
            end do 
        endif 
        
        ! set A and h (includes ghost cells)
        if (init_h_homo) then 

            print *, ""
            print *, "Using homogenous thickness for initial values"

            do j = 1-gc_T, nxT+gc_T
                do i = 1-gc_T, nyT+gc_T
                    if (dist_T(i,j) < 0) then 
                        ! point is outside the ice pack
                        h_T(i,j) = 0
                        A_T(i,j) = 0
                    else
                        h_T(i,j) = h_init
                        A_T(i,j) = min(1.0,(dx/2 + dist_T(i,j))/dx)
                    end if
                end do
            end do

        else 

            print *, ""
            print *, "Using varying thickness field for initial values"

            do j = 1-gc_T, nxT+gc_T
                do i = 1-gc_T, nyT+gc_T
                    if (dist_T(i,j) < 0) then 
                        ! point is outside the ice pack
                        h_T(i,j) = 0
                        A_T(i,j) = 0
                    else
                        if (x_T(i) <= x_extent/2) then 

                            alpha = abs(x_T(i)**2 - x_T(i)*y_T(j) + x_T(i)*x_extent/2)/(sqrt(2.)*abs(x_T(i)))

                            h_T(i,j) = 1.1 + sin(C*alpha + pi/2)
                            A_T(i,j) = min(1.0,(dx/2 + dist_T(i,j))/dx)
                        else

                            ! xtilde = x_T(i) - x_extent/2 ! these were never used 
                            ! ytilde = y_T(j) - x_extent/2

                            alpha = abs(x_T(i)**2 - x_T(i)*y_T(j) + x_T(i)*x_extent/2)/(sqrt(2.)*abs(x_T(i)))

                            h_T(i,j) = 1.1 - sin(C*alpha + pi/2)
                            A_T(i,j) = min(1.0,(dx/2 + dist_T(i,j))/dx)
                        end if 
                    end if
                end do
            end do

        end if 

        open(unit = 1, file = './output/h_init.dat', status = 'unknown')
        open(unit = 2, file = './output/A_init.dat', status = 'unknown')
        call csv_write_dble_2d(1, h_T)
        call csv_write_dble_2d(2, A_T)
        close(1)
        close(2)
    
    end subroutine get_idealized_init_cond_dist

!===========================================================================================!
!                           Set Toy Velocity field                                          !
!===========================================================================================!
    subroutine toy_velocity_field(ugrid, vgrid, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    nxU, nyU, nxV, nyV)

        !=======================================================================================
        ! This subroutine creates a toy initial velocity that is used to validate the calculations 
        ! of C_w and Delta (visc cals); this was needed as we couldn't tell if the derivatives 
        ! were working correctly with all zero velocities. 
        !
        ! NOTE: This velocity field does not represent any physical solution or even 
        ! mathematical solution...it was purely implemented for test purposes.
        !=======================================================================================

        !==================!
        ! ------IN ------- !
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxU, nyU, nxV, nyV,     &
            nU_pnts, nV_pnts
        integer(kind = int_kind), dimension ((nxU+2*gc_U)*(nyU+2*gc_U)), intent (in) :: &
            indxUi, indxUj
        integer(kind = int_kind), dimension ((nxV+2*gc_V)*(nyV+2*gc_V)), intent (in) :: &
            indxVi, indxVj

        !==================!
        ! ---- IN/OUT ---- !
        !==================!
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent (inout) :: &
            ugrid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent (inout) :: &
            vgrid 

        !==================!
        ! ---- LOCAL  ---- !
        !==================!
        integer(kind = int_kind) :: &
            ij, i, j, splitu, splitv

        splitu = nxU/2 + 1
        splitv = nyV/2 + 1
        print *, "Loading toy initial velocity field."

        ! U-points !
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            if (i < splitu) then
                ugrid(i,j) = (0.2/x_extent)*x_u(i)    
            else
                ugrid(i,j) = (0.2/x_extent)*x_u(splitu) - (0.2/x_extent)*(x_u(i)-x_u(splitu))
            end if
        end do

        ! V-points !
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            if (j < splitv) then
                vgrid(i,j) = (0.2/y_extent)*y_v(j)
            else
                vgrid(i,j) = (0.2/y_extent)*y_v(splitv) - (0.2/y_extent)*(y_v(j) - y_v(splitv))
            end if
        end do
    end subroutine toy_velocity_field

!===========================================================================================!
!                 Initialize Distance function With Stopping Criteria                       !
!===========================================================================================!
    subroutine init_dist_fnctn_stp_crit(dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN) 

        !=======================================================================================
        ! This subroutine calculates the distance function for our given domain and terminates
        ! according to the stopping criteria given below.
        !
        ! The used stopping criteria is: 
        !       Sum(abs(phi^n - phi^(n-1)))/M  < beta*delta_T*dx*dx/x_extent^2
        !
        !       - where the sum is over points where the gradient isn't equal to zero 
        !               (avoiding points that haven't updated yet as they are too far from the boundary)
        !       - M is the number of points in the sum
        !
        ! I decided to store the value of the distance function at the node points, because 
        ! depending on the order of the bilinear interpolation, the resulting value at boundaries
        ! could end up being +/- 4.547 e-13 or 0. This normally wouldn't be an issue, but it was 
        ! causing consistency problems as I was interpolating the distance function to the node 
        ! points in multiple places in the code, often with a different order. This resulted in 
        ! issues with the node halo points. By storing the value in this routine, the interpolation
        ! will only be done once.
        !
        !=======================================================================================

        !==================!
        ! ------IN ------- !
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxT, nyT, nxN, nyN              ! indices limits for T points and N points

        !==================!
        ! ---- IN/OUT ---- !
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (inout) :: &
            dist_T, dist_gx_T,  dist_gy_T   ! distance function at T points and gradients
        real(kind = dbl_kind), dimension(nxN, nyN), intent (inout) :: &
            dist_N

        !==================!
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: &
            i, j, iter_count,               &   ! local indices and counters
            M                                   ! number of points used in stopping criteria 

        integer(kind = int_kind), parameter :: &
            max_iter    = 300000                 ! max iteration

        real(kind = dbl_kind), parameter :: &
            eps_dist    = dx,               &   ! used to define a smooth sign function
            dt_dist     = dx/10_dbl_kind,   &   ! "time step" used. This is purely numerical. See Sussman 1994
            beta        = 1.5                   ! used in stopping criteria  

        real(kind = dbl_kind) :: &
            stop_crit, sum,                 &   ! used in determining stopping criteria
            a, b, c, d,                     &   ! used to calc derivatives
            a_neg, b_neg, c_neg, d_neg,     &   ! used in distance value updates
            a_pos, b_pos, c_pos, d_pos                               
                  
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) ::  &
            dist_T_prev,                    &   ! stores previous iterations values
            sign_T                              ! smooth sign function

        real(kind = dbl_kind), dimension(1:max_iter) :: &
            iter_update                         ! stores the new sum/M value at each iteration for convergence study


        ! -----Determine Stopping Criteria and Initialize Values ----- !

            M = 0
            sum = 0
            iter_count = 0

            stop_crit = beta*dt_dist*dx*dx/(x_extent**2)

        ! -----Initialize Distance Function----- !

            do i = 1-gc_T, nxT+gc_T
                do j = 1-gc_T, nyT+gc_T

                    if (imsk(i,j)) then
                        ! if cell doesn't have ice, initialize to -dx/2

                        dist_T(i, j) = -dx/2
                        sign_T(i, j) = dist_T(i, j)/sqrt(dist_T(i, j)**2 + eps_dist**2) 

                    else
                        ! the cell is an interior point. Set distance value to dx/2 
                        
                        dist_T(i, j) = dx/2
                        sign_T(i, j) = dist_T(i, j)/sqrt(dist_T(i, j)**2 + eps_dist**2) 

                    endif

                enddo
            enddo

        ! ---------------------- Solve Distance Function at T points ---------------------------- !
            do iter_count = 1, max_iter
                ! store previous iteration
                dist_T_prev = dist_T

                ! update values everywhere
                do i = 1-gc_T, nxT+gc_T
                    do j = 1-gc_T, nyT+gc_T

                    !------------ Using scheme in Sussman 1994 ------------------------!
                    ! NOTE: the if statements check for boundary conditions at the edge 
                    ! domain (including ghost cells)
                    !------------------------------------------------------------------!
                        !------------ Calc X derivatives -------------------!
                        if (i == 1-gc_T) then
                            a = (dist_T_prev(i+1, j) - dist_T_prev(i, j))/dx
                        else
                            a = (dist_T_prev(i, j) - dist_T_prev(i-1, j))/dx
                        endif

                        if (i == nxT+gc_T) then 
                            b = (dist_T_prev(i, j) - dist_T_prev(i-1, j))/dx
                        else
                            b = (dist_T_prev(i+1, j) - dist_T_prev(i, j))/dx
                        endif   

                        !------------ Calc Y derivatives -------------------!
                        if (j == 1-gc_T) then 
                            c = (dist_T_prev(i, j+1) - dist_T_prev(i, j))/dx
                        else
                            c = (dist_T_prev(i, j) - dist_T_prev(i, j-1))/dx
                        endif

                        if (j == nyT+gc_T) then
                            d = (dist_T_prev(i, j) - dist_T_prev(i, j-1))/dx
                        else
                            d = (dist_T_prev(i, j+1) - dist_T_prev(i, j))/dx
                        endif

                        a_pos = max(0.0_dbl_kind,a)
                        b_pos = max(0.0_dbl_kind,b)
                        c_pos = max(0.0_dbl_kind,c)
                        d_pos = max(0.0_dbl_kind,d)

                        a_neg = min(0.0_dbl_kind,a)
                        b_neg = min(0.0_dbl_kind,b)
                        c_neg = min(0.0_dbl_kind,c)
                        d_neg = min(0.0_dbl_kind,d)

                        if (dist_T(i,j) > 0) then 
                            dist_T(i,j) = dist_T_prev(i,j) + dt_dist*sign_T(i,j)*(1 - sqrt(max((a_pos**2), (b_neg**2)) + max((c_pos**2), (d_neg**2))))
                        elseif (dist_T(i,j) < 0) then
                            dist_T(i,j) = dist_T_prev(i,j) + dt_dist*sign_T(i,j)*(1 - sqrt(max((a_neg**2), (b_pos**2)) + max((c_neg**2), (d_pos**2))))
                        else
                            dist_T(i,j) = dist_T_prev(i,j)
                        endif
                            
                    enddo
                enddo

                !--------------------Store new gradient and calculate stopping criteria ------------------------!
                do i = 1-gc_T, nxT+gc_T
                    do j = 1-gc_T, nyT+gc_T
                        !------------ Calc X derivatives -------------------!
                        if (i == 1-gc_T) then
                            a = (dist_T(i+1, j) - dist_T(i, j))/dx
                        else
                            a = (dist_T(i, j) - dist_T(i-1, j))/dx
                        endif

                        if (i == nxT+gc_T) then 
                            b = (dist_T(i, j) - dist_T(i-1, j))/dx
                        else
                            b = (dist_T(i+1, j) - dist_T(i, j))/dx
                        endif   

                        !------------ Calc Y derivatives -------------------!
                        if (j == 1-gc_T) then 
                            c = (dist_T(i, j+1) - dist_T(i, j))/dx
                        else
                            c = (dist_T(i, j) - dist_T(i, j-1))/dx
                        endif

                        if (j == nyT+gc_T) then
                            d = (dist_T(i, j) - dist_T(i, j-1))/dx
                        else
                            d = (dist_T(i, j+1) - dist_T(i, j))/dx
                        endif

                        dist_gx_T(i,j) = (a+b)/2
                        dist_gy_T(i,j) = (c+d)/2

                        !---------------- Update Sum at points where the gradient isn't 0 -------------------------!
                        if (sqrt(dist_gx_T(i,j)**2 + dist_gy_T(i,j)**2) == 0) then
                            ! don't include point in stopping criteria test !
                        else 
                            sum = sum + abs(dist_T(i,j) - dist_T_prev(i,j))
                            M = M + 1
                        endif

                    enddo
                enddo

                iter_update(iter_count) = sum/M

                !----------------- Check Stopping Criteria if enough iterations have happened-------------------!
                if (iter_count >= 50) then 
                    if (sum/M < stop_crit) then
                        ! stopping criteria met, terminate outer do loop !
                        print *, 'Distance function initialized in ', iter_count, 'iterations'
                        EXIT

                    elseif (iter_count == max_iter) then
                        ! max iteractions met, terminate outer do loop !
                        print *, 'WARNING'
                        print *, 'Distance function failed to converge in the maximum allowable iterations'
                        EXIT

                    else
                        ! continue
                    endif
                else
                    ! continue
                endif

                ! reset sum and M
                sum = 0
                M = 0
            enddo
                
            ! Set distance values at node points !
            do i = 1, nxN
                do j = 1, nyN
                    dist_N(i,j) = (dist_T(i,j) + dist_T(i,j-1) + dist_T(i-1,j-1) + dist_T(i-1,j))/4
                end do 
            end do

                !---------------- Store Distance Values ---------------------!
                ! open(unit = 1, file = '~/Documents/1_Thesis_Work/Model/Dist_Tests/CornerPts_Tests/dist_T_20km_3110it.txt', status = 'unknown')
                ! open(unit = 2, file = '~/Documents/1_Thesis_Work/Model/Dist_Tests/CornerPts_Tests/iter_update_20km.txt', status = 'unknown')
                ! call csv_write_dble_2d(1, dist_T)
                ! call csv_write_dble_1d(2, iter_update)
    end subroutine init_dist_fnctn_stp_crit
end module initialization
