! This is the main driver for the Sea Ice Dynamics solver.
! Original Author: Clinton Seinen

program JFNKsolver

! -------------------------------------------------------------------------------------------------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams             ! includes physical, numerical, and grid parameters
    use initialization          ! includes initialization routines and grid parameters that are defined according to dx and will remain unchanged
    use forcing                 ! includes routines for updating forcing data
    use domain_routines         ! includes routines to limit the domain and set BCs
    use solver_routines         ! includes routines that are the main parts of the actual solver (i.e. Jacobian Mult)
    use var_routines            ! includes routines that calculate variables like the drag coefficent and viscosities
    use csv_file                ! module for writing data to csv format
    use validation_routines     ! module containing some major validation routines
    use validsoln_routines      ! module containing calculations for the exact solution for the validation problem

! ------------------------------------------------------------------------------------------------------------------------------------------------------------------!
    
implicit none
    
    character(len = 8) :: &
        fmt1, fmt2, fmt3,   &   ! formats for printing necessary numbers
        fmt4, fmt5, fmt6,   &   ! 
        dx_f, dt_f,         &   ! strings for holding dx and dt in km and sec 
        config,             &   ! string for holding config numbers   
        t_lvl,              &   ! time level in string format
        eps_str,            &   ! holds the epsilon value in string format
        nltolcoef_str,      &   ! holds the non-linear tolerance, disctol_coef_nl, in string format
        strng                   ! general string

    real(kind = dbl_kind) :: &
        time,       &   ! initial time value
        end_time,   &   ! final time value
        res_Final       ! stores the final residual at each JFNK solve
        
    integer(kind = int_kind) :: &
        nxT, nyT,   &   ! limit of indices for tracer points
        nxN, nyN,   &   ! limit of indices for node points
        nxU, nyU,   &   ! limit of indices for U points
        nxV, nyV,   &   ! limit of indices for V points
        nt,         &   ! number of time steps
        nU_pnts,    &   ! number of u points to be included in the computational domain
        nV_pnts,    &   ! number of v points to be included in the computational domain
        nT_pnts,    &   ! number of T points to be included in the computational domain
        nN_pnts,    &   ! number of N points to be included in the computational domain
        nAdvVols,   &   ! number of N points needed for advection schemes (as locations for control volumes)
        si,         &   ! will be used to store (nU_pnts + nV_pnts), the number of velocitiy points included in the comp. domain.
        tmp_size,   &   ! determines the si of gmres_tmp in the gmres routines
        i, j, ij,   &   ! local counters and indices
        n,          &   ! current time level
        iter_count, &   ! stores the number of iterations required for the non-linear solver
        num_tsteps      ! stores the number of time steps

    integer(kind = int_kind), allocatable, dimension (:) ::  &
        indxUi,     indxUj,         &   ! grid locations of u points in the comp. domain
        indxVi,     indxVj,         &   ! grid locations of v points in the comp. domain
        indxTi,     indxTj,         &   ! grid locations of T points in the comp. domain
        indxNi,     indxNj,         &   ! grid locations of N points in the comp. domain
        indxAdvVi,  indxAdvVj,      &   ! grid locations of N points needed for advection schemes (as locations for control volumes)
        NL_iters                        ! array to store the number of non-linear iterations at each solve

        !----------------------------------------------------------------------!
        ! NOTE: The arrays, indxUi and indxUj (an all other indx vectors
        ! are allocated once to be the maximum size needed even though it 
        ! often won't be needed (unless the full domain is covered in ice).
        !----------------------------------------------------------------------!
            
    real(kind = dbl_kind), allocatable, dimension (:) :: &
        Au,         res_NL,     u_update,       &   ! Matrix vector product, non linear residual, velocity update
        b,          b_0,        vpb_forc,       &   ! the RHS vector of the Au = b equation, and the corresponding linear and non-linear parts. If the validation problem is activated vpb_forc will contain the known forcing vector
        NLRes_Fin,  Vol                             ! arrays to store the final residual and volume at each timestep 

        !----------------------------------------------------------------------!
        ! NOTE: the solution update and residual vectors are allocated once to be the 
        ! max size needed. The full extent of the vectors may not be needed at 
        ! each time step (unless the full domain is covered in ice). At each time
        ! step I will only send a portion of these vectors to the needed computational
        ! routines. 
        !   - Note that this approach will also be applied to the gmres_tmp array
        !----------------------------------------------------------------------!

    integer(kind = int_kind), allocatable, dimension (:,:,:) :: &
        ulmsk,      vlmsk,      &               ! land mask for u and v grid points
        uimsk,      vimsk                       ! ice extent mask for u and v grid points

        !----------------------------------------------------------------------!
        ! NOTE: The above masks are used to enforce our boundary conditions 
        !       automatically using the distance function. The third dimension 
        !       is necessary as multiple (surrounding) velocity points are used 
        !       in the matrix vector product or in the calculation of delta when
        !       determining the viscosity.
        !
        ! NOTE: as of January 26th, I've removed the previous time-steps ice masks for
        !       both u and v; I originally thought these may be required to calculate b_0, 
        !       but I don't believe they are.
        !----------------------------------------------------------------------!
        
    real(kind = dbl_kind), allocatable, dimension (:,:) :: &
        ugrid,      vgrid,                                  &   ! u and v velocity components on the grid - m/s
        uResgrid,   vResgrid,                               &   ! residual on the grid
        uocn_u,     uocn_v,                                 &   ! u component of ocean current - m/s (needed at u and v grid points)
        vocn_u,     vocn_v,                                 &   ! v component of ocean current - m/s (needed at u and v grid points)
        uwnd_u,     uwnd_v,                                 &   ! u component of wind - m/s (needed at u and v grid points)
        vwnd_u,     vwnd_v,                                 &   ! v component of wind - m/s (needed at u and v grid points)
        h_u,        h_v,        h_T,                        &   ! thickness values for the four type of grid points - meters
                                A_T,                        &   ! sea ice concentration at T and N grid points - fraction
        P_u,        P_v,        P_T,            P_N,        &   ! ice strength at T and N grid points - N/m
        zeta_u,     zeta_v,                                 &   ! bulk viscosity - N*s/m^2
        zeta_gx_u,                                          &   ! x component of the bulk visc grad (only needed at U points)
                    zeta_gy_v,                              &   ! y component of the bulk visc grad (only needed at V points)
        eta_u,      eta_v,                                  &   ! shear viscosity - N*s/m^2
        eta_gx_u,   eta_gx_v,                               &   ! x component of the shear visc grad at U and V points 
        eta_gy_u,   eta_gy_v,                               &   ! y component of the shear visc grad at U and V points
        Cw_u,       Cw_v,                                   &   ! non-linear drag coef
                                dist_T,         dist_N,     &   ! distance to the ice pack boundary
                                dist_gx_T,                  &   ! x component of gradient of distance function solution (tracer points)
                                dist_gy_T,                  &   ! y component of gradient of distance function solution (tracer points)
                                dist_gx_T_prev,             &   ! x component of gradient of distance function solution from the previous timestep (tracer points)
                                dist_gy_T_prev                  ! y component of gradient of distance function solution from the previous timestep (tracer points)

    logical, allocatable, dimension (:,:) :: &
        prevUDom,   prevVDom                                ! previous timestep computational domain for the u and v grid 

    fmt1 = '(I4.4)'
    fmt2 = '(I8.8)'
    fmt3 = '(I2.2)'
    fmt4 = '(ES6.0)'
    fmt5 = '(F4.2)'
    write (dx_f, fmt1) int(dx/1000)                 ! turn dx into a string 
    write (dt_f, fmt1) int(dt)                      ! turn dt into a string 
    write (config, fmt3) domainconfig               ! turn config number into a string
    write (eps_str, fmt4) eps                       ! turn epsilon value into a string
    write (nltolcoef_str, fmt4) disctol_coef_nl     ! turn disctol_coef_nl into a string 
    write (strng, fmt5) ResInc_tol

!===========================================================================================!
!                           Initialization Portion                                          !
!===========================================================================================!
! NOTE: at a later date, these can likely be compiled into one initialization subroutine.
    time        = 0.d0 
    num_tsteps  = T/dt

    allocate(NL_iters(num_tsteps), NLRes_Fin(num_tsteps), Vol(0:num_tsteps)) ! 0 added to vol to allow for storage of initial volume
    NLRes_Fin   = 0.d0
    NL_iters    = 0

    print *, "****************************************"
    print *, ""
    print *, "Using configuration ", domainconfig
    print *, ""
    print *, "Spatial Resolution (meters) set to:", dx
    print *, "Temporal Resolution (seconds) set to:", dt
    print *, ""
    print *, "Progressing until ", T, "seconds. Or ", num_tsteps, "timesteps"
    print *, ""
    print *, "Maximum Non-Linear Iterations = ", k_max
    print *, ""
    print *, "Initial Damping Factor = ", tau_init
    print *, ""
    print *, "Discretization Error Cut-Off Coef =", disctol_coef_nl
    if (Adaptive_Damp .and. plateau_check) then 
        print *, "Both Adaptive Damping and Plateau Checking activated"
        print *, "Please choose one. Program terminating."
        print *, "see modelparams.f90"
        stop 
    end if 
    if (AD_plateau_check .and. .not. Adaptive_Damp) then 
        print *, "Adaptive Damping not activated yet its associated plateau check is."
        print *, "Either activate Adaptive damping or turn off its plateau check (AD_plateau_check in modelparams)."
        print *, "Progam terminating."
        stop 
    end if 
    if (plateau_check) then 
        print *, ""
        print *, "Residual plateau tolerance =", plat_tol
        print *, "Solver to check for plateauing and res increase after", platinc_check, "iterations."
        print *, ""
    else 
        print *, ""
    end if 
    if (Adaptive_Damp .and. ResInc_Term) then 
        print *, "Both ResInc_Damp and ResInc_Term are activated."
        print *, "Please choose one. Program Terminating."
        print *, "See modelparams.f90"
        stop
    else if (Adaptive_Damp) then 
        print *, "Using adaptive damping on the Newton Iterations"
        print *, "Increase Factor = ", tau_incfact
        print *, "Decrease Factor = ", tau_decfact
        print *, "Tau Limit = ", tau_lim
        if (AD_plateau_check) then
            print *, "Plateau checking activated."
            print *, "Tolerance = ", AD_plat_tol
            print *, "Mininum iterations before plateau check = ", platinc_check
        end if 
    else if (ResInc_Term) then 
        print *, "Using early, conditional, termination of the JFNK solver if large residual increase is noted!"
        print *, "Residual Increase Tolerance = ", ResInc_tol
    else
        print *, "No Conditional Termination or Damping Used."
    end if 
    print *, ""
    print *, "Jacobian approx eps =", eps
    if (second_ord_Jac) then 
        print *, "Using 2nd order approximation"
    else
        print *, "Using 1st order approximation"
    end if
    if (dist_correct) then 
        print *, ""
        print *, "Distance function to be corrected every", distcrrctn, "time-steps."
        print *, "Max iteration count for correction =", distcrrctn_lim
    else 
        print *, ""
        print *, "Distance function correction turned off."
    end if
    if (smooth_vels) then 
        print *, ""
        print *, "Smoothing of velocity solutions activated."
        print *, "Hyper-viscosity parameter =", visc_smth
        print *, ""
    else
        print *, ""
        print *, "No smoothing of velocities used."
        print *, ""
    end if 
    print *,"*****************************************"

    !====================================================!
    !   Determine grid limits and allocate/init arrays   !
    !====================================================!
        call set_grid_limits_and_initialize(ugrid, vgrid, uResgrid, vResgrid, uocn_u, uocn_v, vocn_u, vocn_v, &     ! initialization module
                                            uwnd_u, uwnd_v, vwnd_u, vwnd_v, h_u, h_v, h_T, A_T,  &
                                            P_u, P_v, P_T, P_N, zeta_u, zeta_v, eta_u, eta_v, Cw_u, Cw_v,  &
                                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                            dist_T, dist_N, dist_gx_T, dist_gy_T, dist_gx_T_prev, dist_gy_T_prev, &
                                            indxUi, indxUj, indxVi, indxVj, prevUDom, prevVDom, &
                                            indxTi, indxTj, indxNi, indxNj, indxAdvVi, indxAdvVj, &
                                            ulmsk, vlmsk, uimsk, vimsk, &
                                            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, &
                                            Au, res_NL, u_update, b, b_0)   

        print *, " "
        print *, 'Grids Set!'
        print *, 'T points:', nxT, nyT
        print *, 'N points:', nxN, nyN
        print *, 'U points:', nxU, nyU
        print *, 'V points:', nxV, nyV
    
    !===============================!
    !   Initialize T grid points    !
    !===============================!
        call init_T_msk(nxT, nyT)                                                           ! initialization module

            print *, " "
            print *, 'Logical masks initialized'

    !===========================================================!
    !   Initialize distance function over the entire domain     !
    !===========================================================!
        call init_dist_fnctn_stp_crit(dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN)     ! initialization module

    !=======================================================================!
    !   Compress index to limit computations to areas where ice is present  !
    !=======================================================================!
        call compress_ind_dist(indxUi, indxUj, indxVi, indxVj, &                            ! domain routine module
                        indxTi, indxTj, indxNi, indxNj, indxAdvVi, indxAdvVj, &
                        nU_pnts, nV_pnts,  nT_pnts, nN_pnts, nAdvVols, &
                        dist_T, dist_N, &
                        nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

            print *, " "
            print *, '*** Domain limited ***'
            print *, "Number of U points considered:    ", nU_pnts
            print *, "Number of V points considered:    ", nV_pnts
            print *, "Number of T points considered:    ", nT_pnts
            print *, "Number of N points considered:    ", nN_pnts

    !===========================!
    !   Set initial conditions  !
    !===========================!
        call get_idealized_init_cond_dist(h_T, A_T, ugrid, vgrid, dist_T, nxT, nyT, nxU, nyU, nxV, nyV, &  ! initialization module
                                            indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), nU_pnts, nV_pnts) 

            print *, " "
            print *, 'Initialized thickness and area fraction set according to the distance function'

            !======================!
            ! Store initial volume !
            !======================!
                Vol(0) = CalcVol(h_T, A_T, nxT, nyT)

    !=======================!
    !   Get forcing data    !
    !=======================!
        call get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time, &                        ! forcing module
                            nxT, nyT, nxU, nyU, nxV, nyV)
         
        call get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time, &                        ! forcing module
                            nxU, nyU, nxV, nyV)

            print *, " "
            print *, 'Wind and Ocean forcing initialized'

    !=======================================================================================!
    !   Create computational masks needed for matrix operations and visc/drag coef calcs    !
    !=======================================================================================!
        call create_comp_msk_dist(ulmsk, uimsk, vlmsk, vimsk, &                             ! domain_routines module
                                ugrid, vgrid, dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                nV_pnts, nU_pnts, nxU, nyU, nxV, nyV, nxN, nyN, nxT, nyT)

    !=======================================================================================!
    !   Interpolate thickness and calculate P, viscosities, and drag coefs in comp domain   !
    !=======================================================================================!
        call interpolate_h(h_u, h_v, h_T, nxU, nyU, nxV, nyV, nxT, nyT, &                   ! var routines module
                            nU_pnts, nV_pnts, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts))

        ! call calc_ice_strgth(P_T, P_N, P_u, P_v, h_T, A_T, dist_T, dist_gx_T, dist_gy_T, &   
        !                     indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
        !                     indxTi(1:nT_pnts), indxTj(1:nT_pnts), indxNi(1:nN_pnts), indxNj(1:nN_pnts), &
        !                     nU_pnts, nV_pnts, nT_pnts, nN_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        call calc_ice_strgth_r02Test(P_T, P_N, P_u, P_v, h_T, A_T, dist_T, dist_gx_T, dist_gy_T, &   
                            indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                            indxTi(1:nT_pnts), indxTj(1:nT_pnts), indxNi(1:nN_pnts), indxNj(1:nN_pnts), &
                            nU_pnts, nV_pnts, nT_pnts, nN_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                    eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                    vlmsk, vimsk, ulmsk, uimsk, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                    nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &   ! var_routines module
                                time, ulmsk, uimsk, vlmsk, vimsk, &
                                nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts))

!===========================================================================================!
!                               Time Stepping                                               !
!===========================================================================================!

    n = 1   ! initialize time step counter !

    do
        if (time >= T) then 
            print *, ""
            print *, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            print *, "Final Time Reached. Solver Terminated"
            print *, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            print *, ""
            Exit
        end if

        ! update time value !
        time    = time + dt 
        write (t_lvl, fmt2) int(time) ! turn time into a string for file naming
        print *, ""
        print *, "============================================================="
        print *, 'Progressing to time:', time
        print *, ""

        ! store si of comp domain (governed by U and V points)
        si = nU_pnts + nV_pnts
        tmp_size = (2*gmres_rest + 1)*si + gmres_rest*(gmres_rest + 9)/2 + 1 ! defined in intel's mkl dev reference

        !******************************** NOTE **********************************************!
        ! subroutines involving matrix vector operations are only passed the size of vectors
        ! according to si, i.e. we send the vectors as b(1:si), Au(1:si), etc.
        !************************************************************************************!

        !===================================================!
        !   Calc b_0 and update forcing to new time level   !  - solver_routines module
        !===================================================!
            call calc_b_0_update_forcing_r02(b_0(1:si), ugrid, vgrid, Cw_u, Cw_v, &
                                        uocn_u, vocn_u, uocn_v, vocn_v, uwnd_u, vwnd_u, uwnd_v, vwnd_v, &
                                        zeta_u, zeta_v, eta_u, eta_v, h_u, h_v, P_T, ulmsk, uimsk, vlmsk, vimsk, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                        nxU, nyU, nxV, nyV, nxT, nyT, nU_pnts, nV_pnts, & 
                                        indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                        time)

            !===============================================!
            !   Add validation problem forcing if needed    !
            !===============================================!
                if (valid_pblm) then 
                    ! print *, "************ Running Validation Problem! ******************"

                    allocate(vpb_forc(nU_pnts + nV_pnts))
                    vpb_forc = 0.

                    ! calculate the additional forcing term (routine located in validation_routines.f90)
                    call calc_validprb_frcng(vpb_forc, time, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, &
                                            nU_pnts, nV_pnts, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts),      &
                                            P_u, P_v, P_T, P_N, h_u, h_v)

                    b_0(1:si) = b_0(1:si) + vpb_forc(1:si)
                    deallocate(vpb_forc)
                end if 

        !===============================================================================!
        !   Using the new forcing, calculate the initial guess for the water drag coef  !   - var_routines module
        !===============================================================================!
            ! Note: for our constant ocean current, this can be commented out
            call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &   
                                    time, ulmsk, uimsk, vlmsk, vimsk, &
                                    nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                    indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts))
        !===========!
        !   Calc b  !   - solver_routines module
        !===========!
            call calc_b(b(1:si), b_0(1:si), Cw_u, Cw_v, &
                        uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, &
                        nU_pnts, nV_pnts, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts))

        !===============!
        !   Calc Au     !   - solver_routines module
        !===============!
            call calc_Au_r02(Au(1:si), ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                            nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                            ulmsk, uimsk, vlmsk, vimsk)

        !===================!
        !   Calc Residual   !
        !===================!
            res_NL = 0.d0
            res_NL = Au - b

        !===================!
        !   Call Solver     ! - solver_routines module 
        !===================!
            call JFNKsolve_r02(ugrid, vgrid, time, Au(1:si), b(1:si), b_0(1:si), u_update(1:si), res_NL(1:si), si, tmp_size, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, & 
                                P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), & 
                                ulmsk, uimsk, vlmsk, vimsk, iter_count, res_Final) 

        !=======================!
        !   Smooth Solution     ! - solver_routines module 
        !=======================!
            if (smooth_vels) then 
                call SmoothUV(time, ugrid, vgrid, uimsk, ulmsk, vimsk, vlmsk, dist_T, dist_gx_T, dist_gy_T, dist_N, & 
                            indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), nU_pnts, nV_pnts, &
                            nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)
            end if 

        !===============================!
        !   Save Principal Stresses     ! - var_routines module
        !===============================!
            if (mod(time, sv_interval) == 0) then
                call CalcSave_Pstress(time, ugrid, vgrid, eta_u, zeta_u, eta_v, zeta_v, P_u, P_v, &
                                indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), nU_pnts, nV_pnts, &
                                uimsk, ulmsk, vimsk, vlmsk, nxU, nyU, nxV, nyV)
            end if 

        !===========================================!
        !   Store NL Iterations and final Residual  !
        !===========================================! 
            NLRes_Fin(n)    = res_Final
            NL_iters(n)     = iter_count
            ! n               = n + 1       

        !===================================!
        !   Place the residual on the grid  !
        !===================================!
            uResgrid = 0.d0

            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                uResgrid(i,j) = res_NL(ij)
            end do             
            
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vResgrid(i,j) = res_NL(nU_pnts + ij)
            end do

        !===================================================!
        !   Store Previous Comp. Domain and Distance Fnctn  !
        !===================================================!
            dist_gx_T_prev  = dist_gx_T 
            dist_gy_T_prev  = dist_gy_T

            call StrUVCmpDomain(prevUDom, prevVDom, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV)

        !===================================================================!
        !   For Test Purposes Only - REMOVE WHEN FULL SOLVER IS RUNNING     !
        !===================================================================!
            ! ugrid = ugrid*10000
            ! vgrid = vgrid*10000

        !===================================================!
        !   Advect Tracers and Correct Distance Function    !
        !===================================================!
            call AdvectTracers(time, ugrid, vgrid, dist_T, dist_gx_T, dist_gy_T, dist_N, h_T, A_T, &
                            indxTi(1:nT_pnts), indxTj(1:nT_pnts), indxAdvVi(1:nAdvVols), indxAdvVj(1:nAdvVols), nT_pnts, nAdvVols, &
                            ulmsk, uimsk, vlmsk, vimsk, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)

            !==================!
            ! Store New Volume !
            !==================!
                Vol(n) = CalcVol(h_T, A_T, nxT, nyT)

        !===============================!
        !   Save solutions if desired   !
        !===============================!
            if (mod(time, sv_interval) == 0) then
                ! save final values !
                open(unit = 1, file = './output/converged_u'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                open(unit = 2, file = './output/converged_v'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                call csv_write_dble_2d(1, ugrid)
                call csv_write_dble_2d(2, vgrid)
                close(1)
                close(2)

                open(unit = 1, file = './output/Res_u'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                open(unit = 2, file = './output/Res_v'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                call csv_write_dble_2d(1, uResgrid)
                call csv_write_dble_2d(2, vResgrid)
                close(1)
                close(2)

                open(unit = 1, file = './output/h_'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                open(unit = 2, file = './output/A_'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                open(unit = 3, file = './output/phi_'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                call csv_write_dble_2d(1, h_T)
                call csv_write_dble_2d(2, A_T)
                call csv_write_dble_2d(3, dist_T)
                close(1)
                close(2)
                close(3)

                print *, ""
                print *, "Converged Solutions Saved"

                if (valid_pblm) then 
                    ! save exact solution !
                    call save_exact(time, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)
                end if 
            end if

        !===================!
        !   Set new domain  !
        !===================!
            call compress_ind_dist(indxUi, indxUj, indxVi, indxVj, indxTi, indxTj, indxNi, indxNj, indxAdvVi, indxAdvVj, &
                                    nU_pnts, nV_pnts,  nT_pnts, nN_pnts, nAdvVols, dist_T, dist_N, &
                                    nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        !=======================================================================================================!
        !   Mask previous time-step values for the new domain and set velocities outside of comp domain to 0    ! 
        !=======================================================================================================!
            call MskPrevTstp(ugrid, vgrid, prevUDom, prevVDom, dist_gx_T_prev, dist_gy_T_prev, &
                            indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT)

        !===============================!
        !   Set new computational masks !
        !===============================!
            call create_comp_msk_dist(ulmsk, uimsk, vlmsk, vimsk, &                             
                                    ugrid, vgrid, dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                    nV_pnts, nU_pnts, nxU, nyU, nxV, nyV, nxN, nyN, nxT, nyT)

        !=======================================================================================!
        !   Interpolate thickness and calculate P, viscosities, and drag coefs in comp domain   !
        !=======================================================================================!
            call interpolate_h(h_u, h_v, h_T, nxU, nyU, nxV, nyV, nxT, nyT, &                   
                            nU_pnts, nV_pnts, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts))

            ! call calc_ice_strgth(P_T, P_N, P_u, P_v, h_T, A_T, dist_T, dist_gx_T, dist_gy_T, &   
            !                     indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
            !                     indxTi(1:nT_pnts), indxTj(1:nT_pnts), indxNi(1:nN_pnts), indxNj(1:nN_pnts), &
            !                     nU_pnts, nV_pnts, nT_pnts, nN_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

            call calc_ice_strgth_r02Test(P_T, P_N, P_u, P_v, h_T, A_T, dist_T, dist_gx_T, dist_gy_T, &   
                                indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                indxTi(1:nT_pnts), indxTj(1:nT_pnts), indxNi(1:nN_pnts), indxNj(1:nN_pnts), &
                                nU_pnts, nV_pnts, nT_pnts, nN_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

            call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                        eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                        vlmsk, vimsk, ulmsk, uimsk, indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts), &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

            call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &   
                                    time, ulmsk, uimsk, vlmsk, vimsk, &
                                    nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                    indxUi(1:nU_pnts), indxUj(1:nU_pnts), indxVi(1:nV_pnts), indxVj(1:nV_pnts))

        !================!
        ! Update counter !
        !================!
            n = n + 1
    end do  

    !===========================================================!
    !   Save NL Iteration Counts, Final Residuals, and Volumes  !
    !===========================================================!
        open(unit = 1, file = './output/NL_iters'//trim(dx_f)//'dt'//trim(dt_f)//'.dat', status = 'unknown')
        call csv_write_integer_1d(1, NL_iters)
        close(1)
        open(unit = 1, file = './output/ConvergedRes_Norms'//trim(dx_f)//'dt'//trim(dt_f)//'.dat', status = 'unknown')
        call csv_write_dble_1d(1, NLRes_Fin)
        close(1)
        open(unit = 1, file = './output/Volumes_dx'//trim(dx_f)//'dt'//trim(dt_f)//'.dat', status = 'unknown')
        call csv_write_dble_1d(1, Vol)
        close(1)

end program JFNKsolver
